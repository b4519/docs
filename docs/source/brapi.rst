###################
Bloc retour API
###################



*********************
General
*********************

This API allows third parties to send edifact messages diretly to Brokercloud.


.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - BR API endpoint
      - https://secure.brokercloud.app/brapi

*********************
Authorization
*********************

All requests should contain the following HTTP headers:

.. code-block:: none

	X-API-KEY: InsertValidApiKeyHere
	X-API-QA: 0


You can request your API key by sending an e-mail to techmaster@brokercloud.info.

**********************
Sending messages
**********************

POST /brapi/messages
=======================

**Parameters**


.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - uid
      - string
      - Required. Identifier of the message
    * - subject
      - string
      - Required. The subject of the message
    * - dateSent
      - datetime
      - Required. The date the message was sent. (ISO-8601)
    * - dateReceived
      - datetime
      - Required. The date the message was received. (ISO-8601)
    * - from
      - int
      - Required. The Portima office id of the sender.
    * - to
      - int
      - Required. The Portima office id of the receiver.
    * - messageClass
      - string
      - Required. The class of the message. (IPC.AS2.E1)
    * - body
      - string
      - Required. The edifact of the message.



**Example request**:

.. sourcecode:: http

	POST /brapi/messages HTTP/1.1
	Host: secure.brokercloud.app
	Content-Type: application/json
	X-API-KEY: InsertValidApiKeyHere
	X-API-QA: 0
	
	{
		"uid":"test",
		"subject":"test",
		"dateSent":"2018-11-12T00:00:43+0200",
		"dateReceived":"2018-11-12T00:00:43+0200",
		"from":"58",
		"to":"88066",
		"messageClass":"IPC.AS2.E1",
		"body":"XGH+1+0058+88066+001'XEH+01+2+0104+++03++++201712010923+E1:::M0104ANN:2'GIS+EW05+6'DTM+002:23032014:001'RFF+001:0240261323'RFF+002:00001'ATT+EW01+07'ATT+A314+3'ATT+A055+1'XRH+1'PTY+006+58:006+++20'XRT+1'XRH+1'PTY+002+6350:006+40546:002'XRT+1'XET+01'XGT+1'"
	}

All dates should be in ISO-8601 format.


**Example response**:

.. sourcecode:: http
	
	HTTP/1.1 200 OK
	Vary: Accept
	Content-Type: text/plain

	OK

If the returned status code is not 200 the message was not successfully sent and the request should be retried.
