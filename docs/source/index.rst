.. Brokercloud docs documentation master file, created by
   sphinx-quickstart on Fri Nov  9 13:15:36 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



.. image:: bclogo.png
   :align: left
   :width: 250px
|
|
|

#################################################
Welcome to the documentation of Brokercloud API's 
#################################################


If you need help you can send an e-mail to techmaster@brokercloud.info or call +3227906060.


Introduction
================

This website documents our different API's and how third parties can interact with them.

.. _BrokerCloud: https://brokercloud.app/

.. toctree::
   :caption: BrokerCloud 
   :maxdepth: 10

   brapi
   restapi






