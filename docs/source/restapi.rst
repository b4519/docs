####################
Rest API
####################

*********************
General
*********************

This API allows third parties to read, create, update and delete data in Brokercloud.



Environments


**ACCEPTANCE**

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - REST API endpoint
      - https://acc.brokercloud.dev/api/v2
    * - Authorization endpoint
      - https://acc.brokercloud.dev/oauth/v2/auth
    * - Token endpoint
      - https://acc.brokercloud.dev/oauth/v2/token

**PRODUCTION**

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - REST API endpoint
      - https://secure.brokercloud.app/api/v2
    * - Authorization endpoint
      - https://secure.brokercloud.app/oauth/v2/auth
    * - Token endpoint
      - https://secure.brokercloud.app/oauth/v2/token


*******************
  Authorization Oauth2
*******************

It's possible to use Oauth2_ for authorization.

.. _Oauth2: https://oauth.net/2/

Client applications will need a client_id and client_secret to be able to access the API. If you did not yet receive your client_id and client_secret please contact techmaster@brokercloud.info. Client applications should also inform us of their redirect_uri (c). Both portal users and broker users are able to request oauth 2.0 access tokens.

Authorization Code Grant Flow
==============================

a) Redirect the user to the Authorization endpoint
---------------------------------------------------

When a user first tries to perform an action that requires authentication, 
the client application will forward the user to the following endpoint:

.. code-block:: none
	
	GET /oauth/v2/auth

The following query parameters are accepted by this endpoint:

.. list-table::
    :header-rows: 1

    * - Parameter
      - Type
      - Description
    * - client_id
      - string
      - The client_id uniquely indentifies the client application.
    * - response_type
      - string
      - Required. The response type. The accepted value is "code".
    * - redirect_uri
      - string
      - Required. The callback URL of the client application.


**Example url**

.. code-block:: none

  GET /oauth/v2/auth?response_type=code&client_id=123abc&redirect_uri=https://client.app

b) User authentication and granting permissions
---------------------------------------------------

After redirecting the user, the Authorization server will prompt the Resource
Owner (user) to enter his login credentials.
After a successful login the user gives permission to client application to access
his resources.

c) Redirection to the client application with authorization code
------------------------------------------------------------------
The Authorization server will redirect the user back to the redirect_uri.

**Access granted**

If the user has granted access to the client application, the Authorization server
appends an additional query parameter "code" to the redirect_uri. This parameter
contains the (temporary) authorization code that client application can
exchange for an access token.

.. code-block:: none

  GET {redirect_uri}?code={code}


**Access not granted**

If the user did not grant access to the client application, the Authorization server
appends an additional query parameter "error" to the redirect_uri. This
parameter contains an error code that explains why access was denied.

.. code-block:: none

  {redirect_uri}?error={error_code}

d) Exchange authorization code for access token
---------------------------------------------------

After the resource owner granted access to the client application in step c, the
client application can use the temporary authorization code to request an
access token.

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - client_id
      - string
      - Required. The client_id received from Brokercloud.
    * - client_secret
      - string
      - Required. The client_secret received from Brokercloud.
    * - grant_type
      - string
      - Required. authorization_code
    * - code
      - string
      - Required. The code received in step c.
    * - redirect_uri
      - string
      - Required. The redirect_uri of the client application.


      
    

**Example request**:

.. code-block:: none

  curl -X POST  --data '{"grant_type": "authorization_code", "client_id": "2_55hpcwmu7xc0okkggocog0wwk00g0kkw00wo0gkosgggg8goos","client_secret": "42zl36b5ydic48kg40kk8c8k0w4wwgwsssskcw44cgw8g8wcsc","code":"ZWZiYWRiOWM3YTdmODE0OWNjNGRkNWQ4ZmQyNWI0ZmIwNWRhYzViNjkzYTRhZDZiZTNmZjk1MmI4MzIxMjlmMw","redirect_uri":"http://www.example.com"}' https://secure.brokercloud.app/oauth/v2/token -H "Content-Type: application/json"

.. sourcecode:: http

  POST /oauth/v2/token HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  
  {
    "grant_type": "authorization_code",
    "client_id": "2_55hpcwmu7xc0okkggocog0wwk00g0kkw00wo0gkosgggg8goos",
    "client_secret": "42zl36b5ydic48kg40kk8c8k0w4wwgwsssskcw44cgw8g8wcsc",
    "code":"ZWZiYWRiOWM3YTdmODE0OWNjNGRkNWQ4ZmQyNWI0ZmIwNWRhYzViNjkzYTRhZDZiZTNmZjk1MmI4MzIxMjlmMw",
    "redirect_uri":"http://www.example.com"
  }

e) Response with access token
---------------------------------

**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "access_token":"NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg",
    "expires_in":3600,
    "token_type":"bearer",
    "scope":null,
    "refresh_token":"MTg1ZTNhMmUyNGVlMmQzMmQyNDJjZmQxZTExM2UyMWE1MTRhZDUwNTFlOTIzZTJiY2IxMTI0NWMzYTJiZTc2ZQ"
  }

The Authorization server will respond with a JSON object that contains the
access token, the token type, the lifetime in seconds and a refresh token. The
access token is used to access API resources whilst the refresh token is used to
request a new access token if the lifetime has been exceeded.


Important: Your application should store both the access and refresh tokens,
without them your user will have to go through the whole authorization process
every time he is accessing the API!


Important: Make sure to keep your client secret in a safe place, in case of loss or
theft it can only be re-generated and it will replace the previous secret. This will
make all previously acquired access and refresh tokens invalid!


Using an Access token to make requests
=========================================

To gain access to any of the APIs resources the Access Token should be send
during every request. This is done by adding it to the Authorization header as
Bearer type.

For example, if the access token is 

.. sourcecode:: none

  NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg

you would make the
request as such:


.. sourcecode:: http

  GET /api/v1/resource HTTP/1.1
  Host: secure.brokercloud.app
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg

Restoring / Renewing an Access Token
======================================

After expiration, invalidation or loss of an access token the client application can
request a new token using the refresh token retrieved in step e. Note that a
refresh token expires after 196 days and the Access token usually expires after
14 days.
To request a new access token the steps are very similar to step d and e.

.. sourcecode:: none

  POST /oauth/v2/token

The request may include the following parameters as JSON in the request body:

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - client_id
      - string
      - Required. The client_id received from Brokercloud.
    * - client_secret
      - string
      - Required. The client_secret received from Brokercloud.
    * - grant_type
      - string
      - Required. refresh_token
    * - refresh_token
      - string
      - Required. The refresh token.
    * - redirect_uri
      - string
      - Required. The redirect_uri of the client application.


The response contains the exact same parameters as specified
earlier in step e.
Important: Any previously acquired access or refresh token will be invalidated
right after requesting these new tokens.


Scopes
================

Scopes have not been documented yet.

****************
Pagination
****************

Resources that return a list of items use pagination. The following GET parameters can be used for pagination:

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - length
      - int
      - Amount of results per page
    * - start
      - int
      - Index of the first element of the page

A maximum of 100 items is returned. Developers can use the "start" and "length" parameters to specify what page they want to see returned. The limit paramter sets the amount of items a page will contain. The start parameter secifies the index of the first item of the page.

This request will return items 1 to 100.

.. sourcecode:: none

  GET /items?start=0&length=100


This request will return items 100 to 200.

.. sourcecode:: none

  GET /items?start=100&length=100

All API endpoints that return a list of data will return data in the following format:

**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json
  {
    "draw": 1, //string specified in the draw get parameter
    "recordsTotal": 10000, //total records
    "recordsFiltered": 3000, //total records after filtering
    "data": [
       // objects
    ]
  }

****************
Filtering
****************
search for test:
&search[value]=test


****************
Ordering
****************
sync by column name ascending:

&columns[0][data]=name
&order[0][column]=0&order[0][dir]=asc


****************
Resources
****************

Parties
==============

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /parties/{id}


Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the party
    * - office_id
      - int
      - Unique id of the brokers office the party belongs to
    * - prefix
      - string
      - First part of the reference of the party. If the reference is A21/1, A is the prefix.
    * - number
      - int
      - The number in the reference of the party. If the reference is A21/1, 21 is the number.
    * - suffix
      - int
      - The last part of the reference of the party. If the reference is A21/1, 1 is the suffix.
    * - alias
      - string
      - The alias of the party. If this is set it is to be used instead of the firstname of the party.
    * - name
      - string
      - The lastname of the party.
    * - firstname
      - string
      - The firstname of the party.
    * - title
      - telebib
      - A102
    * - corporation_or_person,
      - telebib
      - A131
    * - website
      - string
      - The website of the party
    * - dob
      - date Y-m-d
      - Date of birth
    * - dod
      - date Y-m-d
      - Date of death
    * - pob
      - string
      - place of birth
    * - vat_number
      - string
      - vat number
    * - establishment_number
      - string
      - establishment number. A company can have multiple establishments. The KBO gives these establishments a number to indetify them.
    * - fsma_number
      - string
      - FSMA number
    * - legal_form
      - telebib
      - A1CS
    * - vat_directive
      - internal code
      - Vat directive
    * - rsz_number
      - string
      - RSZ number
    * - sex
      - telebib
      - A124
    * - profession
      - string
      - Profession
    * - picture_id
      - int
      - File id of the picture
    * - fax
      - string
      - Fax
    * - fax_description
      - string
      - Description of the fax number
    * - language
      - telebib
      - A10C
    * - national_number
      - string
      - National number
    * - identity_card_valid_from
      - date Y-m-d
      - Identity card valid from
    * - identity_card_valid_until
      - date Y-m-d
      - Identity card valid until
    * - identity_card_number
      - string
      - Identity card number
    * - passport_number
      - string
      - Passport number
    * - marital_status
      - telebib
      - A123
    * - legal_status
      - telebib
      - A130
    * - social_status
      - telebib
      - A132
    * - financial_position
      - telebib
      - A007
    * - drivers_license_issue_date
      - date Y-m-d
      - Driver's license issue date
    * - drivers_license_valid_until
      - date Y-m-d
      - Driver's license valid until
    * - drivers_license_number
      - string
      - Driver's license number
    * - drivers_license_country
      - string
      - Driver's license country
    * - driver_since
      - date Y-m-d
      - Driver since
    * - account_manager
      - Brokercloud User
      - Account manager
    * - client_executive
      - Brokercloud User
      - Client executive
    * - insurance_advisor
      - Brokercloud User
      - Insurance advisor
    * - claims_advisor
      - Brokercloud User
      - Claims advisor
    * - sub_producer
      - int
      - party_id of the sub producer
    * - portfolio
      - insurance portfolio
      - Insurance portfolio
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the party was opened or edited
    * - last_contact_date
      - date Y-m-d
      - Set by the broker
    * - contact_frequency
      - internal code
      - How often the party should be contacted by the broker
    * - vip
      - bool
      - VIP
    * - added_by
      - Brokercloud user
      - The user that created the party
    * - added_time
      - date Y-m-d H:i:s
      - The time the party was created
    * - edited_by
      - Brokercloud user
      - The user that last edited the party
    * - edited_time
      - date Y-m-d H:i:s
      - The time the party was last edited
    * - nationality
      - telebib
      - A121
    * - party_group
      - bool
      - Is the party a group or not
    * - mifid
      - bool
      - Did the party signed the mifid documents
    * - commercial_actions
      - bool
      - Does the party wish to be targeted for commercial actions
    * - active
      - bool
      - Is the party active
    * - fiscal_status
      - telebib
      - A175
    * - phones
      - array
      - Phone numbers
    * - emails
      - array
      - E-mail addresses
    * - addresses
      - array
      - Addresses
    * - types
      - array
      - Party types
    * - relationships
      - array
      - Relationships
    * - account_numbers
      - array
      - Account numbers
    * - members
      - array
      - members of a group
    * - drivers_license_categories
      - array
      - Driver's license categories
    * - nacebels
      - array
      - Nacebels
    * - paritaire_comites
      - array
      - Paritaire comités
    * - producer_numbers
      - array
      - Producer numbers. A company can have multiple producer numbers.


Phone numbers
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the phonenumber
    * - search_key
      - string
      - Numeric representation of the phone number [0-9]
    * - number
      - string
      - Phone number
    * - mobile
      - bool
      - Mobile number
    * - description
      - string
      - Description of the phone number
    * - primary
      - bool
      - Primary


E-mail addresses
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the e-mail address
    * - email 
      - string
      - E-mail address
    * - description
      - string
      - Description of the e-mail address
    * - primary
      - bool
      - Primary

Addresses
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the address
    * - street 
      - string
      - Street
    * - number 
      - string
      - Number
    * - bus 
      - string
      - Bus
    * - extra
      - string
      - Extra
    * - city 
      - string
      - city
    * - postal_code 
      - string
      - Postal_code
    * - country 
      - string
      - Country
    * - primary 
      - bool
      - Primary address
    * - invoices 
      - bool
      - Use this address for invoicing
    * - correspondece 
      - bool
      - Use this address for correspondence
    * - description
      - string
      - Description of the address

Party types
^^^^^^^^^^^^^^^^^^

(Policy holder, Insurer, Accountant,...)

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - id of the type
    * - type
      - string
      - Type

Relationships
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - party_id
      - int
      - The party_id of the relationship
    * - type 
      - string
      - Relation type of the current party
    * - relation_type 
      - string
      - Relation type of the relationship
    * - portal_access 
      - bool
      - The current party has access to the data of the relationship in the customer portal

Account numbers
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the account number
    * - iban
      - string
      - IBAN
    * - bic 
      - string
      - BIC
    * - description 
      - string
      - Description of the account number

Driver's license categories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - type
      - telebib
      - A192
    * - valid_from
      - date Y-m-d
      - valid from
    * - valid_until
      - date Y-m-d
      - Valid until

Members
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - party_id
      - int
      - party_id of the member of the group
    * - head
      - bool
      - This party is the head of the group

Nacebels
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - code
      - string
      - Nacebel-code

Paritaire comités
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - code
      - string
      - Code

Producer numbers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the producer number
    * - number
      - string
      - producer number of the company
    * - description
      - string
      - Description of the producer number

Portal users
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the user
    * - email
      - string
      - email of the user
    * - date_created
      - date Y-m-d H:i:s
      - date the user was created
    * - active
      - bool
      - the user is active or not
    * - active_from
      - date Y-m-d
      - the user is active since this date
    * - active_until
      - date Y-m-d
      - the user is inactive since this date

GET
------

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - changed_since
      - unix timestamp
      - the policy changed since this date


**Headers**

The HTTP header X-OFFICE-ID is required for this endpoint.

**Example request**:

.. sourcecode:: http

  GET /parties/1 HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg
  
  
**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "id":1,
    "office_id": 1,
    "prefix":"A",
    "number":"21",
    "suffix":"1",
    "name":"Hinckxt",
    "alias":null,
    "firstname":"Lothar",
    "title": {"code":"1", "description":"Mijnheer"},
    "corporation_or_person": {"code":"1", "description":"Natuurlijk persoon"},
    "website":"www.brokercloud.info",
    "dob":"1994-01-01",
    "dod":null,
    "pob":"Mechelen",
    "vat_number":null,
    "establishment_number":null,
    "fsma_number":null,
    "legal_form":null,
    "vat_directive":null,
    "rsz_number":null,
    "sex":{"code":"1", "description":"Mannelijk"},
    "profession":null,
    "picture_id":1,
    "fax":null,
    "fax_description":null,
    "language":{"code":"2", "description":"Nederlands"},
    "national_number": null,
    "identity_card_valid_from":null,
    "identity_card_valid_until":null,
    "identity_card_number":null,
    "passport_number":null,
    "marital_status": {"code":"2", "description":"Ongehuwd"},
    "legal_status": null,
    "social_status": null,
    "financial_position": null,
    "drivers_license_issue_date": null,
    "drivers_license_valid_until": null,
    "drivers_license_number": null,
    "drivers_license_country": null,
    "driver_since": null,
    "account_manager":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "client_executive":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "insurance_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "claims_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "sub_producer":1,
    "portfolio":{"id":1, "name":"Mijn portefeuille"},
    "last_activity":"2019-01-01",
    "last_contact_date":"2019-01-01",
    "contact_frequency":null,
    "vip":1,
    "added_by":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "added_time":"2019-01-01",
    "edited_by":null,
    "edited_time":null,
    "nationality": {"code":"B", "description":"Belgische"},
    "party_group":0,
    "mifid":1,
    "commercial_actions":1,
    "active":1,
    "fiscal_status": null,
    "phones": [{"id":1,"search_key":"035500000", "number":"035500000", "mobile":0, "description":"Our phone number", "primary":1}],
    "emails": [{"id":1,"email":"techmaster@brokercloud.info", "description":"Our E-mail address", "primary":1}],
    "addresses": [{"id":1,"street":"Sint-Amandsesteenweg", "number":"92", "bus":"", "extra":"", "postal_code":"2880", "city":"Bornem", "country_code":"B", "description":"Our address", "primary":1, "correspondence":1, "invoices":1}],
    "types":[{"id":61, "type":"Verzekeringsnemer"}],
    "relationships": [{"party_id":2, "type":"Echtgenoot", "relation_type":"Echtgenote"}],
    "account_numbers":[],
    "members": [],
    "drivers_license_categories":["type":"B", "valid_from":"2010-01-01", "valid_until":"2050-01-01"],
    "nacebels":["code":"62010"],
    "paritaire_comites":[],
    "producer_numbers":[]
  }




GET V2
------

With the parameters below Brokercloud will be able to give a an empty list of parties, a single party or multiple parties as response.

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - firstname
      - string
      - The firstname of the party.
    * - name
      - string
      - The lastname of the party.
    * - dob
      - date Y-m-d
      - Date of birth

POST
------

With this API call it's possible to create or update parties in Brokercloud. The idea is to be as complete as possible with the data for the party, but it's already possible to create parties with a minimum set of elements. One party is created with one POST call, multiple parties will require multiple calls. When a party is created in Brokercloud the API will response a "http 201 CREATED" message containing the the party_id and all fields of the GET PARTY in the body, when a party is updated the response has the same body and will have a 'http 200 OK'.

**Important notes:**

* To update and existing party just include the id field (when this field is empty a new party will automatically be created)
* The POST body has some object arrays such as phone numbers an email adresses. Including an empty array in an update POST will remove the existing data, if you want to update only a single element in an object array we expect always to include the ones that should not be updated.
* As a general rule empty arrays will remove existing values in that array. For example nacebel numbers, drivers license arrays,...

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /parties

Below is de description of the request body elements data that are to be sent to Brokercloud for this POST call. Some elements are mandaroty while other are conditional, both can be depending on the reason of the post (create/update party).
Just as for the GET call data elements the elements in this POST call can also consist of multiple levels (object arrays). This will make it possible to manage phone numbers, adresses, emails, party types, relationsships, etc...


Party fields
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - Unique id of the party. Mandatory in case of update. In case of 'new' party don't include the field.
    * - prefix
      - string
      - O
      - 
      - First part of the reference of the party. If the reference is A21/1, A is the prefix.
    * - number
      - int
      - O
      - 
      - The number in the reference of the party. If the reference is A21/1, 21 is the number.
    * - suffix
      - int
      - O
      - 
      - The last part of the reference of the party. If the reference is A21/1, 1 is the suffix.
    * - alias
      - string
      - O
      - 
      - The alias of the party. If this is set it is to be used instead of the firstname of the party.
    * - name
      - string
      - O in case of update, else M
      - 
      - The lastname of the party. Mandatory for 'new' party. For existing party it's optional.
    * - firstname
      - string
      - O in case update, else M
      - 
      - The firstname of the party. Mandatory for 'new' party. For existing party it's optional.
    * - title
      - string (telebib code)
      - O
      - 
      - A102
    * - corporation_or_person,
      - string (telebib code)
      - O
      - 
      - A131
    * - website
      - string
      - O
      - 
      - The website of the party
    * - dob
      - date Y-m-d
      - O
      - 
      - Date of birth
    * - dod
      - date Y-m-d
      - O
      - 
      - Date of death
    * - pob
      - string
      - O
      - 
      - place of birth
    * - vat_number
      - string
      - O
      - 
      - vat number
    * - establishment_number
      - stringHTTP
      - O
      - 
      - establishment number. A company can have multiple establishments. The KBO gives these establishments a number to indetify them.
    * - fsma_number
      - string
      - O
      - 
      - FSMA number
    * - legal_form
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A1CS
      - A1CS
    * - vat_directive
      - internal code
      - O
      - see table below
      - Vat directive
    * - rsz_number
      - string
      - O
      - 
      - RSZ number
    * - sex
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A124
      - A124
    * - profession
      - string
      - O
      - 
      - Profession
    * - picture_id
      - int
      - O
      - 
      - File id of the picture.
    * - fax
      - string
      - O
      - 
      - Fax
    * - fax_description
      - string
      - O
      - 
      - Description of the fax number
    * - language
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A10C
      - A10C
    * - national_number
      - string
      - O
      - 
      - National number
    * - identity_card_valid_from
      - date Y-m-d
      - O
      - 
      - Identity card valid from
    * - identity_card_valid_until
      - date Y-m-d
      - O
      - 
      - Identity card valid until
    * - identity_card_number
      - string
      - O
      - 
      - Identity card number
    * - passport_number
      - string
      - O
      - 
      - Passport number
    * - marital_status
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A10C
      - A123
    * - legal_status
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A130
      - A130
    * - social_status
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A132
      - A132
    * - financial_position
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A007
      - A007
    * - drivers_license_issue_date
      - date Y-m-d
      - O
      - 
      - Driver's license issue date
    * - drivers_license_valid_until
      - date Y-m-d
      - O
      - 
      - Driver's license valid until
    * - drivers_license_number
      - string
      - O
      - 
      - Driver's license number
    * - drivers_license_country
      - string
      - O
      - 
      - Driver's license country
    * - driver_since
      - date Y-m-d
      - O
      - 
      - Driver since
    * - account_manager
      - Brokercloud User
      - O
      - user_id 
      - Account manager
    * - client_executive
      - Brokercloud User
      - O
      - user_id 
      - Client executive
    * - insurance_advisor
      - Brokercloud User
      - O
      - user_id 
      - Insurance advisor
    * - claims_advisor
      - Brokercloud User
      - O
      - user_id 
      - Claims advisor
    * - sub_producer
      - int
      - O
      - user_id 
      - party_id of the sub producer
    * - portfolio
      - int
      - O
      - portfolio_id
      - Insurance portfolio
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the party was opened or edited
    * - last_contact_date
      - date Y-m-d
      - O
      - 
      - Set by the broker
    * - contact_frequency
      - internal code
      - O
      - see list below
      - How often the party should be contacted by the broker
    * - vip
      - bool
      - O
      - 
      - VIP
    * - added_by
      - Brokercloud user
      - O
      - user_id
      - The user that created the party
    * - edited_by
      - Brokercloud user
      - O
      - user_id
      - The user that last edited the party
    * - nationality
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A121
      - A121
    * - party_group
      - bool
      - O
      - 
      - Is the party a group or not
    * - mifid
      - bool
      - O
      - 
      - Did the party signed the mifid documents
    * - commercial_actions
      - bool
      - O
      - 
      - Does the party wish to be targeted for commercial actions
    * - active
      - bool
      - O
      - 
      - Is the party active
    * - fiscal_status
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A175 
      - A175
    * - phones
      - array
      - O
      - see table below
      - Phone numbers
    * - emails
      - array
      - O
      - see table below
      - E-mail addresses
    * - addresses
      - array
      - O
      - see table below
      - Addresses
    * - types
      - array of internal codes
      - M (at least one)
      - see list below
      - Party types
    * - relationships
      - array
      - O
      - see table below
      - Relationships
    * - account_numbers
      - array
      - O
      - see table below
      - Account numbers
    * - members
      - array
      - M if party_group = true
      - see table below
      - members of a group
    * - drivers_license_categories
      - array
      - O
      - see table below
      - Driver's license categories
    * - nacebels
      - array
      - O
      - Any possible Nacebel code of 5 digits
      - Nacebels
    * - paritaire_comites
      - array of nacebel codes
      - O
      - see nacebel site
      - Paritaire comités
    * - producer_numbers
      - array
      - O
      - 
      - Producer numbers. A company can have multiple producer numbers.

VAT directive codes
^^^^^^^^^^^^^^^^^^

* code "19" = Btw-plichtig
* code "21" = Buiten EU
* code "20" = Intracommunautaire btw-plichtigheid
* code "25" = Medecontractant
* code "24" = Niet btw-plichtig
* code "22" = Particulier
* code "18" = Zaakvoerder


Contact frequency codes
^^^^^^^^^^^^^^^^^^

* code "1" = Geen
* code "2" = Wekelijks
* code "3" = Maandelijks
* code "4" = Trimesterieel
* code "5" = Semesterieel
* code "6" = Jaarlijks
* code "7" = 2 Jaarlijks
* code "8" = 3 Jaarlijks


Party type codes
^^^^^^^^^^^^^^^^^^

* code "486" = Aanbrenger
* code "1" = Advocaat
* code "6944" = Andere mij. verz.
* code "6948" = Andere mij.
* code "7450" = Bankklant
* code "3149" = Begunstigde
* code "6988" = Behandelend geneesheer
* code "170" = Bestuurder
* code "98" = Binex lead
* code "3" = Boekhouder
* code "2861" = Borgsteller
* code "454" = Curator
* code "6" = Deurwaarder
* code "169" = Eigenaar
* code "7" = Expert
* code "458" = Geneesheer
* code "462" = Gerechtsdeskundige
* code "11600" = Geschenk
* code "6976" = Gewonde (buiten verzekerd voertuig)
* code "6972" = Gewonde (voertuig derde)
* code "161" = Hersteller
* code "6980" = Huurder
* code "466" = Informant
* code "5335" = Kredietklant
* code "470" = Leverancier
* code "2841" = Medelener
* code "10982" = medicus
* code "6984" = Mij slachtoffer
* code "8" = Notaris
* code "11" = Overige
* code "11864" = Pensioen
* code "6968" = Politie
* code "474" = Procureur
* code "6956" = Prospect
* code "13" = Rechtbank
* code "12" = Rechter/Commissaris
* code "3045" = Schadebeheerder
* code "478" = Schuldbemiddelaar
* code "97" = Schuldbemiddelde
* code "14" = Schuldeiser
* code "16" = Sociaal secretariaat
* code "2" = Subproducent
* code "9" = Syndicus
* code "17" = Tegenpartij
* code "7404" = TEST
* code "6960" = Tussenkomende overheid
* code "490" = Verzekerde
* code "61" = Verzekeringsmaatschappij
* code "159" = Verzekeringsmaatschappij tegenpartij
* code "60" = Verzekeringsmakelaar
* code "5" = Verzekeringsnemer
* code "9074" = vicky
* code "482" = Vrijwillig tussenkomend
* code "99" = Website lead

Country Codes
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Code
      - Country
    * - A
      - Oostenrijk
    * - AE
      - Verenigde Arabische Emiraten
    * - AF
      - Afghanistan
    * - AG
      - Antigua en Barbuda
    * - AI
      - Anguilla
    * - AL
      - Albanië
    * - AM
      - Armenië
    * - AN
      - Nederlandse Antillen
    * - AND
      - Andorra
    * - AO
      - Angola
    * - AQ
      - Antarctica
    * - AR
      - Argentinië
    * - AS
      - Amerikaans Samoa
    * - AUS
      - Australië
    * - AW
      - Aruba
    * - AX
      - Aland Eilanden
    * - AZ
      - Azerbaijan
    * - B
      - België
    * - BB
      - Barbados
    * - BD
      - Bangladesh
    * - BF
      - Burkina Faso
    * - BG
      - Bulgarije
    * - BH
      - Bahrein
    * - BI
      - Burundi
    * - BIH
      - Bosnië-Herzegovina
    * - BJ
      - Benin
    * - BL
      - Saint Barthélemy
    * - BM
      - Bermuda
    * - BN
      - Brunei
    * - BO
      - Bolivia
    * - BQ
      - Bonaire, Sint Eustatius en Saba
    * - BR
      - Brazilië
    * - BS
      - Bahamas
    * - BT
      - Bhutan
    * - BV
      - Bouvet eiland
    * - BW
      - Botswana
    * - BY
      - Belarus
    * - BZ
      - Belize (Brits Honduras)
    * - CAM
      - Kameroen
    * - CC
      - Cocoseiland
    * - CDN
      - Canada
    * - CF
      - Centraal Afrikaanse Republiek
    * - CH
      - Zwitserland
    * - CI
      - Ivoorkust
    * - CK
      - Cook Eilanden
    * - CN
      - China
    * - CO
      - Colombia
    * - COD
      - Congo, Democratische Republiek van de
    * - CR
      - Costa Rica
    * - CS
      - Servië en Montenegro
    * - CU
      - Cuba
    * - CV
      - Kaapverdië
    * - CW
      - Curaçao
    * - CX
      - Christmaseilanden
    * - CY
      - Cyprus
    * - CZ
      - Tsjechische Republiek
    * - D
      - Duitsland
    * - DJ
      - Djibouti
    * - DK
      - Denemarken
    * - DM
      - Dominica
    * - DO
      - Dominicaanse Republiek
    * - DZ
      - Algerije
    * - E
      - Spanje
    * - EAT
      - Tanzanië
    * - EAU
      - Oeganda
    * - EC
      - Ecuador
    * - EG
      - Egypte
    * - EH
      - West Sahara
    * - ER
      - Eritrea
    * - ETH
      - Ethiopië
    * - EW
      - Estland
    * - F
      - Frankrijk
    * - FJ
      - Fiji
    * - FK
      - Falklandeilanden (Malvinas)
    * - FL
      - Liechtenstein
    * - FM
      - Micronesië
    * - FO
      - Faeroër eilanden
    * - GAB
      - Gabon
    * - GB
      - Verenigd Koninkrijk
    * - GBZ
      - Gibraltar
    * - GD
      - Grenada
    * - GE
      - Georgië
    * - GF
      - Frans Guyana
    * - GG
      - Guernsey
    * - GH
      - Ghana
    * - GL
      - Groenland
    * - GP
      - Guadaloupe
    * - GQ
      - Equatoriaal Guinea
    * - GR
      - Griekenland
    * - GS
      - Zuid-Georgië en de Zuid-Sandwicheilanden
    * - GT
      - Guatemala
    * - GU
      - Guam
    * - GW
      - Guinea-Bissau
    * - GY
      - Guyana
    * - H
      - Hongarije
    * - HK
      - Hong Kong
    * - HM
      - Heardeiland en McDonaldeilanden
    * - HN
      - Honduras
    * - HR
      - Kroatië
    * - HT
      - Haïti
    * - I
      - Italië
    * - IL
      - Israël
    * - IM
      - Isle of Man
    * - IND
      - India
    * - IO
      - British Indian Ocean Territory
    * - IQ
      - Irak
    * - IR
      - Iran
    * - IRL
      - Ierland
    * - IS
      - IJsland
    * - J
      - Japan
    * - JE
      - Jersey
    * - JM
      - Jamaica
    * - JO
      - Jordanië
    * - KE
      - Kenia
    * - KG
      - Kyrgystan (Kirgizië)
    * - KH
      - Cambodja
    * - KI
      - Kiribati
    * - KM
      - Comoren
    * - KN
      - Saint Kitts en Nevis Anguilla
    * - KP
      - Korea-Noord
    * - KW
      - Koeweit
    * - KY
      - Kaaiman Eilanden
    * - KZ
      - Kazachstan
    * - L
      - Luxemburg
    * - LA
      - Laos
    * - LAR
      - Libië
    * - LB
      - Libanon
    * - LC
      - Sint Lucië
    * - LK
      - Sri Lanka
    * - LR
      - Liberië
    * - LS
      - Lesotho
    * - LT
      - Litouwen
    * - LV
      - Letland
    * - M
      - Malta
    * - MA
      - Marokko
    * - MC
      - Monaco
    * - MD
      - Moldavië
    * - ME
      - Montenegro
    * - MEX
      - Mexico
    * - MF
      - Saint Martin
    * - MH
      - Marshall Eilanden
    * - MK
      - Macedonië
    * - MM
      - Myanmar
    * - MN
      - Mongolië
    * - MO
      - Macau
    * - MOC
      - Mozambique
    * - MP
      - Noordelijke Mariana Eilanden
    * - MQ
      - Martinique
    * - MS
      - Montserrat
    * - MU
      - Mauritius
    * - MV
      - Maldiven
    * - MW
      - Malawi
    * - MY
      - Maleisië
    * - N
      - Noorwegen
    * - NC
      - Nieuw-Caledonië
    * - NF
      - Norfolk Eiland
    * - NI
      - Nicaragua
    * - NL
      - Nederland
    * - NP
      - Nepal
    * - NR
      - Nauru
    * - NU
      - Niue
    * - NZ
      - Nieuw-Zeeland
    * - OM
      - Oman
    * - P
      - Portugal
    * - PA
      - Panama
    * - PE
      - Peru
    * - PF
      - Frans Polynesië
    * - PG
      - Papua Nieuw Guinea
    * - PH
      - Filipijnen
    * - PK
      - Pakistan
    * - PL
      - Polen
    * - PM
      - Sint Pierre en Miquelon
    * - PN
      - Pitcairn Eiland
    * - PR
      - Puerto Rico
    * - PS
      - Palestijnse Bezette Gebieden
    * - PW
      - Palau
    * - PY
      - Paraguay
    * - QA
      - Qatar
    * - RC
      - Taiwan
    * - RCA
      - Centraal Afrikaanse Republiek
    * - RCB
      - Kongo
    * - RCH
      - Chili
    * - RE
      - Reunion
    * - RG
      - Guinea
    * - RI
      - Indonesië
    * - RIM
      - Mauritanië
    * - RM
      - Madagascar
    * - RMM
      - Mali
    * - RN
      - Niger
    * - RO
      - Roemenië
    * - ROK
      - Zuid-Korea
    * - RS
      - Republiek Servië
    * - RSM
      - San Marino
    * - RU
      - Rusland
    * - RWA
      - Ruanda
    * - S
      - Zweden
    * - SA
      - Saoedi-Arabië
    * - SB
      - Solomon Eilanden
    * - SC
      - Seychellen
    * - SD
      - Soedan
    * - SF
      - Finland
    * - SGP
      - Singapore
    * - SH
      - Sint Helena, Ascension en Tristan da Cunha
    * - SJ
      - Svalbard en Jan Mayen
    * - SK
      - Slowakije
    * - SLO
      - Slovenië
    * - SN
      - Senegal
    * - SO
      - Somalië
    * - SR
      - Suriname
    * - SS
      - Zuid Soedan
    * - ST
      - Sao Tome & Principe
    * - SUD
      - Soedan
    * - SV
      - El Salvador
    * - SWA
      - Namibië
    * - SX
      - Sint-Maarten
    * - SY
      - Syrië
    * - SZ
      - Swaziland
    * - TC
      - Turkse en Caicos Eilanden
    * - TCH
      - Tsjaad
    * - TF
      - Franse Zuidelijke Gebieden
    * - TG
      - Togo
    * - TH
      - Thailand
    * - TJ
      - Tadjikistan
    * - TK
      - Tokelau
    * - TL
      - Oost Timor
    * - TM
      - Turkmenistan
    * - TN
      - Tunesië
    * - TO
      - Tonga
    * - TP
      - Oost Timor
    * - TR
      - Turkije
    * - TT
      - Trinidad en Tobago
    * - TV
      - Tuvalu
    * - UA
      - Oekraïne
    * - UM
      - Verenigde Staten ver uit de kust gelegen kleinere eilanden
    * - USA
      - Verenigde Staten
    * - UY
      - Uruguay
    * - UZ
      - Oezbekistan
    * - V
      - Vatikaanstad
    * - VC
      - Sint Vincent en de Grenadines Eilanden
    * - VE
      - Venezuela
    * - VG
      - Maagdeneilanden (Britse)
    * - VI
      - Maagdeneilanden (V.S.)
    * - VN
      - Vietnam
    * - VU
      - Vanuatu
    * - WAG
      - Gambia
    * - WAL
      - Sierra Leone
    * - WAN
      - Nigeria
    * - WF
      - Wallis en Futuna Eilanden
    * - WS
      - Samoa
    * - XK
      - Kosovo
    * - XX
      - Andere
    * - YE
      - Jemen
    * - YT
      - Mayotte
    * - YU
      - Joegoslavië
    * - Z
      - Zambia
    * - ZA
      - Zuid-Afrika
    * - ZW
      - Zimbabwe


Phone numbers
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - Unique id of the phonenumber
    * - number
      - string
      - M
      - 
      - Phone number
    * - mobile
      - bool
      - M
      - 
      - Mobile number
    * - description
      - string
      - O
      - 
      - Description of the phone number
    * - primary
      - bool
      - M
      -
      - Primary


E-mail addresses
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - Unique id of the e-mail address
    * - email 
      - string
      - M
      - 
      - E-mail address
    * - description
      - string
      - O
      - 
      - Description of the e-mail address
    * - primary
      - bool
      - M
      - 
      - Primary

Addresses
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      -
      - Unique id of the address
    * - street 
      - string
      - M
      - 
      - Street
    * - number 
      - string
      - M
      - 
      - Number
    * - bus 
      - string
      - O
      - 
      - Bus
    * - extra
      - string
      - O
      - 
      - Extra
    * - city 
      - string
      - M
      - 
      - city
    * - postal_code 
      - string
      - M
      - 
      - Postal_code
    * - country 
      - string
      - M
      - 
      - Country
    * - country_code 
      - string
      - M
      - 
      - Country code
    * - primary 
      - bool
      - M
      - 
      - Primary address
    * - invoices 
      - bool
      - M
      - 
      - Use this address for invoicing
    * - correspondece 
      - bool
      - M
      - 
      - Use this address for correspondence
    * - description
      - string
      - O
      - 
      - Description of the address



Relationships
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      -
      - The id of the relationship
    * - party_id1
      - int
      - M in case of update, else O
      -
      - The party_id of the current party
    * - party_id2
      - int
      - M
      -
      - The party_id of the relationship
    * - relation_type1
      - string
      - M
      -
      - Relation type of the current party
    * - relation_type2
      - string
      - M
      - 
      - Relation type of the relationship
    * - portal_access 
      - bool
      - M
      - 
      - The current party has access to the data of the relationship in the customer portal
    * - start_date 
      - date Y-m-d
      - M
      - 
      - The relationship start date
    * - end_date 
      - date Y-m-d
      - O
      - 
      - The relationship end date

Account numbers
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - unique id of the account number
    * - iban
      - string
      - M
      - 
      - IBAN
    * - bic 
      - string
      - O
      - 
      - BIC
    * - description 
      - string
      - O
      - 
      - Description of the account number

Driver's license categories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - type
      - string (telebib code)
      - M
      - 
      - A192
    * - valid_from
      - date Y-m-d
      - M
      - 
      - valid from
    * - valid_until
      - date Y-m-d
      - M
      - 
      - Valid until

Members
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - party_id
      - int
      - M
      - 
      - party_id of the member of the group
    * - head_of_group
      - bool
      - M
      - 
      - This party is the head of the group


Producer numbers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      -
      - Unique id of the producer number
    * - number
      - string
      - M
      -
      - producer number of the company
    * - description
      - string
      - O
      -
      - Description of the producer number


**Example request**:

.. sourcecode:: http
  
  POST /parties HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg
        
  { 
    "alias":"Frans",
    "firstname":"Frans Gérard Louis",
    "name":"Gendarme",
    "title":"1",
    "corporation_or_person":"1",
    "website":"www.voorbeeld.be",
    "dob":"1989-01-01",
    "pob":"Mechelen",
    "sex":"1",
    "language":"2",
    "marital_status":"2",
    "nationality":"B",
    "mifid":1,
    "commercial_actions":1,
    "active":1,
    "phones":[ 
        { 
          "number":"035500000",
          "mobile":0,
          "description":"Our phone number",
          "primary":1
        }
    ],
    "emails":[ 
        { 
          "email":"techmaster@brokercloud.info",
          "description":"Our E-mail address",
          "primary":1
        }
    ],
    "addresses":[ 
        { 
          "street":"Sint-Amandsesteenweg",
          "number":"92",
          "postal_code":"2880",
          "city":"Bornem",
          "description":"Standaard adres",
          "primary":1,
          "correspondence":1,
          "invoices":1
        }
    ],
    "types":[ 
        "5",
        "170",
        "490"
    ],
    "relationships":[ 
        { 
          "party_id":2,
          "type":"Echtgenoot",
          "relation_type":"Echtgenote"
        }
    ],
    "drivers_license_categories":[ 
        { 
          "type":"B",
          "valid_from":"2010-01-01",
          "valid_until":"2050-01-01"
        }
    ],
    "nacebels":[ 
        "62010"
    ]
  }

**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "id":1,
    "office_id": 1,
    "prefix":"A",
    "number":"21",
    "suffix":"1",
    "name":"Hinckxt",
    "alias":null,
    "firstname":"Lothar",
    "title": {"code":"1", "description":"Mijnheer"},
    "corporation_or_person": {"code":"1", "description":"Natuurlijk persoon"},
    "website":"www.brokercloud.info",
    "dob":"1994-01-01",
    "dod":null,
    "pob":"Mechelen",
    "vat_number":null,
    "establishment_number":null,
    "fsma_number":null,
    "legal_form":null,
    "vat_directive":null,
    "rsz_number":null,
    "sex":{"code":"1", "description":"Mannelijk"},
    "profession":null,
    "picture_id":1,
    "fax":null,
    "fax_description":null,
    "language":{"code":"2", "description":"Nederlands"},
    "national_number": null,
    "identity_card_valid_from":null,
    "identity_card_valid_until":null,
    "identity_card_number":null,
    "passport_number":null,
    "marital_status": {"code":"2", "description":"Ongehuwd"},
    "legal_status": null,
    "social_status": null,
    "financial_position": null,
    "drivers_license_issue_date": null,
    "drivers_license_valid_until": null,
    "drivers_license_number": null,
    "drivers_license_country": null,
    "driver_since": null,
    "account_manager":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "client_executive":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "insurance_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "claims_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "sub_producer":1,
    "portfolio":{"id":1, "name":"Mijn portefeuille"},
    "last_activity":"2019-01-01",
    "last_contact_date":"2019-01-01",
    "contact_frequency":null,
    "vip":1,
    "added_by":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "added_time":"2019-01-01",
    "edited_by":null,
    "edited_time":null,
    "nationality": {"code":"B", "description":"Belgische"},
    "party_group":0,
    "mifid":1,
    "commercial_actions":1,
    "active":1,
    "fiscal_status": null,
    "phones": [{"id":1,"search_key":"035500000", "number":"035500000", "mobile":0, "description":"Our phone number", "primary":1}],
    "emails": [{"id":1,"email":"techmaster@brokercloud.info", "description":"Our E-mail address", "primary":1}],
    "addresses": [{"id":1,"street":"Sint-Amandsesteenweg", "number":"92", "bus":"", "extra":"", "postal_code":"2880", "city":"Bornem", "description":"Our address", "primary":1, "correspondence":1, "invoices":1}],
    "types":[{"id":61, "type":"Verzekeringsnemer"}],
    "relationships": [{"party_id":2, "type":"Echtgenoot", "relation_type":"Echtgenote"}],
    "account_numbers":[],
    "members": [],
    "drivers_license_categories":["type":"B", "valid_from":"2010-01-01", "valid_until":"2050-01-01"],
    "nacebels":["code":"62010"],
    "paritaire_comites":[],
    "producer_numbers":[]
  }




Policies
===========

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /policies/{id}


Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - office_id
      - int
      - Unique id of the brokers office the policy belongs to
    * - group_policy
      - bool
      - Policy is a group policy (if used see Group & fleet policy fields)
    * - group_quote
      - bool
      - Policy is a group policy quote (if used see Group & fleet policy fields)
    * - fleet
      - bool
      - Policy is a fleet (group) policy (if used see Group & fleet policy fields)
    * - policy_version_id
      - int
      - Unique id of the endorsement (used for updates, omitted for new endorsements)
    * - policy_id
      - int
      - unique id of the policy.
    * - group_policy_id
      - int
      - id of the group policy (used to link endorsement to a group policy)
    * - number
      - string
      - policy number
    * - brokers_reference
      - string
      - Policy reference of the broker
    * - endorsement_number
      - string
      - Endorsement number
    * - endorsement_reason
      - string
      - Reason for creating the endorsement
    * - status
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A003
    * - state_of_acceptance
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A055
    * - state_of_project_acceptance
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A056
    * - quote
      - bool
      - Policy is a quote
    * - contract_inception_date
      - date Y-m-d
      - Contract inception date
    * - inception_date
      - date Y-m-d
      - Inception date
    * - expiry_date
      - date Y-m-d
      - Expiry date
    * - main_renewal_date
      - date d/m
      - Main renewal date
    * - next_premium_installment
      - date Y-m-d
      - Date of next premium notification
    * - last_premium
      - date Y-m-d
      - Date of last premium notification
    * - first_commission
      - date Y-m-d
      - Date of first commission
    * - last_commission
      - date Y-m-d
      - Date of last commission
    * - cancellation_date
      - date Y-m-d
      - Cancellation date
    * - suspension_date
      - date Y-m-d
      - Suspension date
    * - termination_date
      - date Y-m-d
      - Termination date
    * - inserted_into_package_date
      - date Y-m-d
      - Inserted into package date
    * - duration_years
      - int
      - Duration years
    * - duration_months
      - int
      - Duration Months
    * - duration_days
      - int
      - Duration days
    * - continuation_duration_years
      - int
      - Continuation duration years
    * - continuation_duration_months
      - int
      - Continuation duration months
    * - continuation_duration_days
      - int
      - Continuation duration days
    * - domain
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A571
    * - type
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A502
    * - product
      - telebib
      - referentiel
    * - renewals_collection_mode
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A600
    * - discharge_collection_mode
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A602
    * - payment_frequency
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A325
    * - currency
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A660
    * - cancellation_motive
      - string
      - Cancellation motive
    * - termination_motive
      - string
      - Termination motive
    * - suspension_motive
      - string
      - Suspension motive
    * - placement_order_status
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A410
    * - co_insuringpolicy
      - bool
      - Co-insuringspolicy
    * - policy_take_over_contract_inception_date
      - date
      - Inception date of the policy take over contract
    * - policy_take_over_expiry_date
      - date
      - Policy take over expiry date
    * - policy_take_over_number
      - string
      - Policy take over number
    * - policy_take_over_fsma
      - string
      - Policy take over FSMA
    * - policy_take_over_insurer_id
      - int
      - Policy take over insurer id
    * - payment_reference
      - string
      - Reference payments
    * - base_premium
      - float
      - base premium
    * - technical_expenses
      - float
      - Technical expenses
    * - commercialization_expenses
      - float
      - Commercialization expenses
    * - net_premium
      - float
      - Net premium
    * - other_costs_to_be_added_to_the_net_premium
      - float
      - Other costs to be added to the net premium
    * - splitting_costs
      - float
      - Splitting costs
    * - costs
      - float
      - Costs
    * - charges
      - float
      - Charges
    * - charges_and_costs
      - float
      - Charges and costs
    * - gross_premium
      - float
      - Gross premium
    * - premium_notification_costs
      - float
      - Premium notification costs
    * - total_to_be_paid
      - float
      - Total to be paid
    * - estimated_acquisition_costs_non_vehicle_liability
      - float
      - Estimated acquisition costs non vehicle liability
    * - estimated_administration_costs_non_vehicle_liability
      - float
      - Estimated administration costs non vehicle liability
    * - commission
      - float
      - Commission
    * - acquisition_commission
      - float
      - Acquisition commission
    * - surrender_value_of_contract_at_end_of_last_year
      - float
      - Surrender value of contract at end of last year
    * - description
      - string
      - Description
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - account_manager
      - int Brokercloud User
      - Account manager
    * - account_manager_group
      - int Brokercloud User
      - Account group manager
    * - account_manager_life
      - int Brokercloud User
      - Account life insurance manager
    * - account_manager_life_group
      - int Brokercloud User
      - Account life insurance group manager
    * - client_executive
      - int Brokercloud User
      - Client executive
    * - insurance_advisor
      - int Brokercloud User
      - Insurance advisor
    * - insurance_advisor_group
      - int Brokercloud User
      - Insurance group advisor
    * - insurance_advisor_life
      - int Brokercloud User
      - Insurance life insurance advisor
    * - insurance_advisor_life_group
      - int Brokercloud User
      - Insurance life insurance group advisor
    * - claims_advisor
      - int Brokercloud User
      - Claims advisor
    * - portfolio_id
      - int insurance portfolio
      - Insurance portfolio
    * - binex_id
      - int
      - Binex id
    * - premium_notifications_via
      - enum(mail|letter)
      - Specification to which channel to receive premium notifications
    * - payment_terms
      - int
      - Payment terms id
    * - reminders_frequency
      - int
      - Reminders frequency id
    * - specific_conditions_file_id
      - int
      - Specific conditions file id
    * - fleet_number
      - string
      - Fleet number
    * - keypoint_declaration_id
      - int
      - Keypoint declaration id
    * - keypoint_case_id
      - int
      - Keypoint case id
    * - production_date
      - date
      - Production date
    * - production_type
      - int
      - Production type id
    * - broker_product
      - int
      - Broker product id      
    * - parties
      - array
      - Policy parties. Parties linked to the policy
    * - risks
      - array
      - Policy risks. Risks linked to the policy
    * - subproducers
      - array
      - Policy subproducers. Supbroducers linked to the policy

Policy loan (domain = 13) additional fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - type_of_loan
      - string
      - https://www.telebib2.org/CodeListsValues.asp?waarde=0200
    * - mortgage_deed_types
      - string
      - https://www.telebib2.org/CodeListsValues.asp?waarde=0220
    * - interest_rate
      - float
      - Interest rate
    * - formula
      - string
      - Formula
    * - mortgage_periodicity
      - string
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A325
    * - loan_reason
      - int
      - Loan reason id
    * - percentage_cap_plus
      - float
      - Percentage cap plus
    * - percentage_cap_min
      - float
      - Percentage cap min
    * - period_pre_withdrawal
      - string
      - Period pre withdrawal
    * - bridging_loan
      - bool
      - Sets this as a bridging loan
    * - amount_borrowed
      - float
      - The borrowed amount
    * - amount_debt
      - float
      - The debt amount
    * - amount_monthly_payment
      - float
      - The monthly payment amount
    * - loan_amount_quotient
      - float
      - The loan amount quotient
    * - maximum_number_of_partial_withdrawals
      - int
      - Maximum number of partial withdrawals
    * - withdrawals_count
      - int
      - Maximum number of withdrawals
    * - mortgage_reference_index
      - int
      - Mortgaga reference index number
    * - mortgage_reference_index_type
      - string
      - https://www.telebib2.org/CodeListsValues.asp?waarde=0410
    * - mortgage_reference_index_periodicity
      - string
      - https://www.telebib2.org/CodeListsValues.asp?waarde=0420
    * - mortgage_reference_index_month
      - int
      - The monthnumber (Januari=1, Februari=2, ...) of the mortgage reference index
      

Group & fleet policy fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - group_policy
      - bool
      - Policy is a group policy
    * - group_quote
      - bool
      - Policy is a group policy quote
    * - fleet
      - bool
      - Policy is a fleet (group) policy
    * - office_id
      - int
      - Unique id of the brokers office the policy belongs to
    * - policy_version_id
      - int
      - Unique id of the endorsement (used for updates, omitted for new policies)
    * - policy_id
      - int
      - unique id of the policy.
    * - number
      - string
      - policy number
    * - endorsement_number
      - string
      - Endorsement number
    * - endorsement_reason
      - string
      - Reason for creating the endorsement
    * - status
      - telebib
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A003
    * - description
      - string
      - Description
    * - account_manager
      - int Brokercloud User
      - Account manager
    * - account_manager_group
      - int Brokercloud User
      - Account group manager
    * - account_manager_life
      - int Brokercloud User
      - Account life insurance manager
    * - account_manager_life_group
      - int Brokercloud User
      - Account life insurance group manager
    * - client_executive
      - int Brokercloud User
      - Client executive
    * - insurance_advisor
      - int Brokercloud User
      - Insurance advisor
    * - insurance_advisor_group
      - int Brokercloud User
      - Insurance group advisor
    * - insurance_advisor_life
      - int Brokercloud User
      - Insurance life insurance advisor
    * - insurance_advisor_life_group
      - int Brokercloud User
      - Insurance life insurance group advisor
    * - claims_advisor
      - int Brokercloud User
      - Claims advisor
    * - portfolio_id
      - int insurance portfolio
      - Insurance portfolio
    * - broker_product
      - int
      - Broker product id      

Policy parties
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the policy_party
    * - party_id
      - int
      - Unique id of the party
    * - type
      - string
      - Type (Policy holder, Company, ...)
    * - participation_insurer
      - int
      - Participation of the insurer in the policy
    * - premium_percentage
      - int
      - Percentage the party will pay (total of all parties must be 100)
    * - reference
      - string
      - Reference party
    * - reference_contact
      - string
      - Reference contact
    * - primary_contact_id
      - int
      - party_id of the primary contact of the party

Policy risks
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - policy_risk_id
      - int
      - M in case of update, else O
      -
      - unique id of the policy risk
    * - risk_version_id
      - int
      - M in case of update, else O
      -
      - unique id of the risk version
    * - risk_id
      - int
      - M in case of update, else O
      -
      - unique id of the risk

General clauses
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the general clause
    * - reference
      - string
      - reference of the general clause
    * - url
      - string
      - url
    * - title
      - string
      - title
    * - url_product_description
      - string
      - url product description


GET
--------

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - policy_version_id
      - int
      - The policy version id.

**Example request**:

.. sourcecode:: http

  GET /policies/{policy_version_id} HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg


**Example response**:

.. sourcecode:: http

  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "id":1,
    "policy_id":1,
    "current_endorsement":1,
    "group_policy_id":null,
    "group_policy":0,
    "office_id":1,
    "number":"123456789",
    "endorsement_number":"0",
    "brokers_reference":"987654321",
    "fleet_number":null,
    "status": {"code":"1", "description":"Lopend"},
    "state_of_acceptance": null,
    "state_of_project_acceptance": null,
    "contract_inception_date": "2019-01-01",
    "expiry_date": "2019-12-31",
    "inception_date": "2019-01-01",
    "main_renewal_date": "01/01",
    "next_premium_installment":null,
    "last_premium":null,
    "first_commission":null,
    "last_commission": null,
    "cancellation_date": null,
    "termination_date": null,
    "suspension_date": null,
    "inserted_into_package_date": null,
    "duration_years": 1,
    "duration_months": 0,
    "duration_days": 0,
    "continuation_duration_years": 1,
    "continuation_duration_months": 0,
    "continuation_duration_days": 0,
    "product": null,
    "domain":{"code":"05", "description":"Motorrijtuigen"},
    "type":{"code":"510", "description":"Toerisme en Zaken, gemengd gebruik"},
    "renewals_collection_mode": null,
    "discharge_collection_mode": null,
    "payment_frequency": null,
    "currency": null,
    "cancellation_motive":null,
    "termination_motive":null,
    "suspension_motive": null,
    "placement_order_status":null,
    "co_insuringpolicy":0,
    "account_manager":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "client_executive":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "insurance_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "claims_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "base_premium":null,
    "technical_expenses":null,
    "commercialization_expenses":null,
    "net_premium":null,
    "splitting_costs":null,
    "charges":null,
    "costs":null,
    "charges_and_costs":null,
    "gross_premium":null,
    "premium_notification_costs":null,
    "estimated_acquisition_costs_non_vehicle_liability":null,
    "estimated_administration_costs_non_vehicle_liability":null,
    "commission":null,
    "acquisition_commission":null,
    "surrender_value_of_contract_at_end_of_last_year":null,
    "total_to_be_paid":null,
    "description": "",
    "last_activity": "2019-01-01",
    "added_by":{"id":1, "fistname":"Lothar", "lastname":"Hinckxt"},
    "edited_time":null,
    "edited_by":null,
    "parties":[{"type":{"code":61,"description":"Verzekeringsnemer"},"party_id":1,"participation_insurer":null,"premium_percentage":null,"reference":null,"reference_contact":null,"primary_contact_id":null}]
    "risks": [{"policy_risk_id": 1,"risk_version_id": 9,"risk_id": 6},{"policy_risk_id": 3,"risk_version_id": 18,"risk_id": 21},{"policy_risk_id": 38,"risk_version_id": 61,"risk_id": 52}]
  }


POST
--------

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /policies

**Example request**:

.. sourcecode:: http

  GET /policies/ HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg
  
  {
    "office_id": 1,
    "current_endorsement": false,
    "group_policy_id": 0,
    "group_policy": false,
    "number": "1234",
    "endorsement_number": 0,
    "brokers_reference": "",
    "status": "1",
    "state_of_acceptance": null,
    "state_of_project_acceptance": null,
    "contract_inception_date": "2018-04-19",
    "expiry_date": null,
    "inception_date": "2018-04-19",
    "main_renewal_date": "0000-04-19",
    "next_premium_installment": null,
    "last_premium": null,
    "first_commission": null,
    "last_commission": null,
    "cancellation_date": null,
    "suspension_date": null,
    "termination_date": null,
    "inserted_into_package_date": null,
    "duration_years": 1,
    "duration_months": 0,
    "duration_days": 0,
    "continuation_duration_years": 1,
    "continuation_duration_months": 0,
    "continuation_duration_days": 0,
    "domain": "05",
    "type": "510",
    "renewals_collection_mode": "3",
    "discharge_collection_mode": "3",
    "payment_frequency": "1",
    "currency": "EUR",
    "cancellation_motive": null,
    "termination_motive": null,
    "suspension_motive": null,
    "placement_order_status": "0",
    "co_insuringpolicy": true,
    "account_manager": null,
    "client_executive": null,
    "insurance_advisor": null,
    "claims_advisor": null,
    "portfolio": null,
    "payment_reference": "1",
    "base_premium": 0,
    "technical_expenses": 0,
    "commercialization_expenses": 0,
    "net_premium": 0,
    "other_costs_to_be_added_to_the_net_premium": 0,
    "splitting_costs": 0,
    "costs": 0,
    "charges": 0,
    "charges_and_costs": 0,
    "gross_premium": 0,
    "premium_notification_costs": 0,
    "total_to_be_paid": 0,
    "estimated_acquisition_costs_non_vehicle_liability": 0,
    "estimated_administration_costs_non_vehicle_liability": 0,
    "commission": 0,
    "acquisition_commission": 0,
    "surrender_value_of_contract_at_end_of_last_year": 0,
    "description": "",
    "last_activity": "2021-07-13 15:17:22",
    "added_by": null,
    "added_time": "2018-04-19 16:11:32",
    "product": null,
    "category": 1,
    "parties": [],
    "general_clauses": [],
    "risks": [
        {
            "policy_risk_id": 1,
            "risk_version_id": 9,
            "risk_id": 6
        },
        {
            "policy_risk_id": 3,
            "risk_version_id": 18,
            "risk_id": 21
        },
        {
            "policy_risk_id": 38,
            "risk_version_id": 61,
            "risk_id": 52
        }
    ],
    "broker_product":"2858"
  }

**Example response**:

.. sourcecode:: http

  HTTP/1.1 201 Created
  Vary: Accept
  Content-Type: application/json

  {
    "policy": {
        "policy_id": "654",
        "policy_version_id": "567",
        "office_id": "1",
        "group_policy_id": null,
        "number": "1234",
        "current_endorsement": "1",
        "brokers_reference": "",
        "endorsement_number": "0",
        "endorsement_reason": "",
        "status_id": "1",
        "state_of_acceptance": null,
        "state_of_project_acceptance": null,
        "quote": "0",
        "contract_inception_date": null,
        "inception_date": null,
        "expiry_date": null,
        "main_renewal_date": null,
        "next_premium_installment": null,
        "last_premium": null,
        "first_commission": null,
        "last_commission": null,
        "cancellation_date": null,
        "suspension_date": null,
        "termination_date": null,
        "inserted_into_package_date": null,
        "duration_years": "1",
        "duration_months": "0",
        "duration_days": "0",
        "continuation_duration_years": "1",
        "continuation_duration_months": "0",
        "continuation_duration_days": "0",
        "domain_id": "05",
        "type_id": "510",
        "renewals_collection_mode": "3",
        "discharge_collection_mode": "3",
        "payment_frequency": "1",
        "currency": "EUR",
        "cancellation_motive": null,
        "termination_motive": null,
        "suspension_motive": null,
        "placement_order_status": "0",
        "co_insuringpolicy": "1",
        "commercial_program_code": null,
        "commercial_program_label": null,
        "commercial_program_reference": null,
        "product": "0",
        "product_id": "0",
        "commercial_program_url": null,
        "policy_take_over_contract_inception_date": null,
        "policy_take_over_expiry_date": null,
        "policy_take_over_number": null,
        "policy_take_over_fsma": null,
        "policy_take_over_insurer_id": null,
        "payment_reference": "1",
        "specific_conditions_file_id": null,
        "production_date": null,
        "production_type": null,
        "broker_product": "2858",
        "account_manager_id": null,
        "account_manager_group_id": null,
        "account_manager_life_id": null,
        "account_manager_life_group_id": null,
        "client_executive_id": null,
        "insurance_advisor_id": null,
        "insurance_advisor_group_id": null,
        "insurance_advisor_life_id": null,
        "insurance_advisor_life_group_id": null,
        "claims_advisor_id": null,
        "account_manager": null,
        "client_executive": null,
        "insurance_advisor": null,
        "claims_advisor": null,
        "portfolio_id": null,
        "domain": "Auto",
        "type": "Toerisme en Zaken, gemengd gebruik ",
        "displayname": "1234\/0",
        "last_activity": "2021-07-23 15:40:46",
        "base_premium": "0.00",
        "technical_expenses": "0.00",
        "commercialization_expenses": "0.00",
        "net_premium": "0.00",
        "other_costs_to_be_added_to_the_net_premium": "0.00",
        "splitting_costs": "0.00",
        "costs": "0.00",
        "charges": "0.00",
        "charges_and_costs": "0.00",
        "gross_premium": "0.00",
        "premium_notification_costs": "0.00",
        "total_to_be_paid": "0.00",
        "estimated_acquisition_costs_non_vehicle_liability": "0.00",
        "estimated_administration_costs_non_vehicle_liability": "0.00",
        "commission": "0.00",
        "acquisition_commission": "0.00",
        "surrender_value_of_contract_at_end_of_last_year": "0.00",
        "group_policy": "0",
        "description": "",
        "fleet": "0",
        "group_quote": "0",
        "binex_id": null,
        "premium_notifications_via": "mail",
        "payment_terms": null,
        "reminders_frequency": null,
        "group_policy_number": null,
        "fleet_number": null,
        "insurance_portfolio": null,
        "type_of_loan": null,
        "mortgage_deed_type": null,
        "interest_rate": null,
        "mortgage_order": null,
        "type_of_loan_review": null,
        "formula": null,
        "loan_periodicity": null,
        "loan_reason": null,
        "percentage_cap_plus": null,
        "percentage_cap_min": null,
        "period_pre_withdrawal": null,
        "bridging_loan": null,
        "amount_borrowed": null,
        "amount_debt": null,
        "amount_monthly_payment": null,
        "loan_amount_quotient": null,
        "maximum_number_of_partial_withdrawals": null,
        "withdrawals_count": null,
        "mortgage_reference_index": null,
        "mortgage_reference_index_type": null,
        "mortgage_reference_index_periodicity": null,
        "mortgage_reference_index_month": null,
        "office_name": "Kantoor X",
        "model_type": "policies",
        "renewals_collection_mode_id": "3",
        "status": "Lopend",
        "parties_per_type": [],
        "parties": [],
        "general_clauses": [],
        "risks": "test ,test666 ,T-001-EST ",
        "insurers": null,
        "policy_holders": null
    }
}
  
PATCH
--------

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /policies

.. |br| raw:: html

    <br>

Policy fields
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - policy_version_id
      - int
      - M
      -
      - Unique id of the endorsement
    * - policy_id
      - int
      - M
      -
      - unique id of the policy.
    * - group_policy_id
      - int
      - M in case of update, else O
      -
      - id of the group policy
    * - group_policy
      - bool
      - M
      -
      - Policy is a group policy
    * - number
      - string
      - M
      -
      - policy number
    * - endorsement_number
      - string
      - O
      -
      - Endorsement number
    * - brokers_reference
      - string
      - O
      -
      - Policy refernce of the broker
    * - fleet_number
      - string
      - O
      -
      - Fleet number
    * - status
      - telebib
      - M
      - 1, 2, 3, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A003
    * - state_of_acceptance
      - telebib
      - O
      - 1, 2, 3, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A055
    * - state_of_project_acceptance
      - telebib
      - O
      - 7, 8, 9, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A056
    * - contract_inception_date
      - date Y-m-d
      - O
      -
      - Contract inception date
    * - expiry_date
      - date Y-m-d
      - O
      -
      - Expiry date
    * - inception_date
      - date Y-m-d
      - O
      -
      - Inception date
    * - main_renewal_date
      - date d/m
      - M
      -
      - Main renewal date
    * - next_premium_installment
      - date Y-m-d
      - O
      -
      - Date of next premium notification
    * - last_premium
      - date Y-m-d
      -
      -
      - Date of last premium notification
    * - first_commission
      - date Y-m-d
      - O
      -
      - Date of first commission
    * - last_commission
      - date Y-m-d
      - O
      -
      - Date of last commission
    * - cancellation_date
      - date Y-m-d
      - O
      -
      - Cancellation date
    * - suspension_date
      - date Y-m-d
      - O
      -
      - Suspension date
    * - termination_date
      - date Y-m-d
      - O
      -
      - Termination date
    * - inserted_into_package_date
      - date Y-m-d
      - O
      -
      - Inserted into package date
    * - duration_years
      - int
      - O
      -
      - Duration years
    * - duration_months
      - int
      - O
      -
      - Duration Months
    * - duration_days
      - int
      - O
      -
      - Duration days
    * - continuation_duration_years
      - int
      - O
      -
      - Continuation duration years
    * - continuation_duration_months
      - int
      - O
      -
      - Continuation duration months
    * - continuation_duration_days
      - int
      - O
      -
      - Continuation duration days
    * - product
      - telebib
      - O
      - Product code as defined in Portima's PAF Module
      - referentiel
    * - domain
      - telebib
      - M
      - 01, 02, 03, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A571
    * - type
      - telebib
      - O
      - 001, 010, 011, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A502
    * - renewals_collection_mode
      - telebib
      - O
      - 3, 4, 5
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A600
    * - discharge_collection_mode
      - telebib
      - O
      - 3, 4
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A602
    * - payment_frequency
      - telebib
      - O
      - 0, 1, 2, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A325
    * - currency
      - telebib
      - O
      - EUR, USD, JPY, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A660
    * - cancellation_motive
      - string
      - O
      -
      - Cancellation motive
    * - termination_motive
      - string
      - O
      -
      - Termination motive
    * - suspension_motive
      - string
      - O
      -
      - Suspension motive
    * - placement_order_status
      - telebib
      - O
      - 0, 1, 2, ...
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A410
    * - co_insuringpolicy
      - bool
      - O
      -
      - Co-insuringspolicy
    * - account_manager
      - Brokercloud User
      - O
      -
      - Account manager
    * - client_executive
      - Brokercloud User
      - O
      -
      - Client executive
    * - insurance_advisor
      - Brokercloud User
      - O
      -
      - Insurance advisor
    * - claims_advisor
      - Brokercloud User
      - O
      -
      - Claims advisor
    * - portfolio
      - insurance portfolio
      - O
      -
      - Insurance portfolio
    * - payment_reference
      - string
      - O
      -
      - Reference payments
    * - base_premium
      - float
      - O
      -
      - base premium
    * - technical_expenses
      - float
      - O
      -
      - Technical expenses
    * - commercialization_expenses
      - float
      - O
      -
      - Commercialization expenses
    * - net_premium
      - float
      - O
      -
      - Net premium
    * - other_costs_to_be_added_to_the_net_premium
      - float
      - O
      -
      - Other costs to be added to the net premium
    * - splitting_costs
      - float
      - O
      -
      - Splitting costs
    * - costs
      - float
      - O
      -
      - Costs
    * - charges
      - float
      - O
      -
      - Charges
    * - charges_and_costs
      - float
      - O
      -
      - Charges and costs
    * - gross_premium
      - float
      - O
      -
      - Gross premium
    * - premium_notification_costs
      - float
      - O
      -
      - Premium notification costs
    * - total_to_be_paid
      - float
      - O
      -
      - Total to be paid
    * - estimated_acquisition_costs_non_vehicle_liability
      - float
      - O
      -
      - Estimated acquisition costs non vehicle liability
    * - estimated_administration_costs_non_vehicle_liability
      - float
      - O
      -
      - Estimated administration costs non vehicle liability
    * - commission
      - float
      - O
      -
      - Commission
    * - acquisition_commission
      - float
      - O
      -
      - Acquisition commission
    * - surrender_value_of_contract_at_end_of_last_year
      - float
      - O
      -
      - Surrender value of contract at end of last year
    * - description
      - string
      - O
      -
      - Description
    * - parties
      - array
      - O
      -
      - Policy parties. Parties linked to the policy
    * - last_activity
      - date Y-m-d H:i:s
      - O
      -
      - The last time the party was opened or edited
    * - added_by
      - Brokercloud user
      - O
      -
      - The user that created the policy
    * - added_time
      - date Y-m-d H:i:s
      - O
      -
      - The time the policy was created
    * - edited_by
      - Brokercloud user
      - O
      -
      - The user that last edited the policy
    * - edited_time
      - date Y-m-d H:i:s
      - O
      -
      - The time the policy was last edited

Policy parties
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      -
      - unique id of the policy_party
    * - party_id
      - int
      - M
      -
      - Unique id of the party
    * - type
      - internal code
      - M
      - See list under POST Parties section.
      - Type (Policy holder, Company, ...)
    * - participation_insurer
      - int
      - O
      -
      - Participation of the insurer in the policy
    * - premium_percentage
      - int
      - O
      -
      - Percentage the party will pay (total of all parties must be 100)
    * - reference
      - string
      - O
      -
      - Reference party
    * - reference_contact
      - string
      - O
      -
      - Reference contact
    * - primary_contact_id
      - int
      - O
      -
      - party_id of the primary contact of the party


General clauses
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      -
      - unique id of the general clause
    * - reference
      - string
      - O
      -
      - reference of the general clause
    * - url
      - string
      - O
      -
      - url
    * - title
      - string
      - O
      -
      - title
    * - url_product_description
      - string
      - O
      -
      - url product description


Claims
==============

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /claims/{id}


Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the claim
    * - office_id
      - int
      - Unique id of the brokers office the claim belongs to
    * - claim_number
      - string
      - Claim number/broker reference
    * - opening_date
      - date Y-m-d
      - Opening date
    * - closure_date
      - date Y-m-d
      - Closure date
    * - event_moment_time
      - time H:i:s
      - time the event happened
    * - event_moment_date
      - date Y-m-d
      - date the event happened
    * - notification_drawn_up_date
      - date Y-m-d
      - Date drawn up
    * - type_of_management
      - telebib
      - C050
    * - state_of_file
      - telebib
      - C051
    * - liability
      - telebib
      - C250
    * - type_of_accident_liability
      - telebib
      - CNA1
    * - liability_established
      - bool
      - Is the liability establised
    * - assessor_is_appointed
      - bool
      - Is the assessor appointed
    * - disbursement_agreement
      - bool
      - Is there an disbursement agreement
    * - payment_request_received_from_third_party
      - bool
      - Is there a received payment request from the third party
    * - legal_action_against_opponent
      - bool
      - Is there a legal action against the opponent
    * - communication_text_insurer
      - string
      - Communication text insurer
    * - opinion_policy_holder_responsibility
      - string
      - Opinion policyholder considering responsibility
    * - event_address
      - array
      - Address where the event happened
    * - police_report_address
      - array
      - Address on the police report
    * - report_number
      - string
      - Number of the police report
    * - report_date
      - date Y-m-d
      - Date of the police report
    * - report_description
      - string
      - Description on the police report
    * - claim_circumstances
      - telebib
      - C221
    * - type_of_party_causing_the_accident
      - telebib
      - CNA2
    * - relationship_policy_holder_and_party_causing_the_accident
      - telebib
      - CNA5
    * - applied_sectoral_agreement
      - telebib
      - C059
    * - origin_of_the_water_damage
      - telebib
      - CM41
    * - type_of_window_glass
      - telebib
      - CM51
    * - manner_of_burglary
      - telebib
      - CM61
    * - written_report
      - bool
      - Is there a written report
    * - witnesses
      - bool
      - Are there witnesses
    * - dead_or_wounded
      - bool
      - Are there dead or wounded
    * - the_declaration_mentions_wounded
      - bool
      - Does the declaration mentions wounded
    * - material_damage
      - bool
      - Is there material damage
    * - material_damages_other_than_to_vehicles
      - bool
      - Are there material damages other than to vecicles
    * - immaterial_damages
      - bool
      - Are there immaterial damages
    * - sectoral_agreement_applied
      - bool
      - Is there a sectoral agreement applied
    * - application_kfk_convention
      - bool
      - Is there an application KFK convention
    * - our_insured_has_a_cl_guarantee
      - bool
      - Insured person has a CL guarantee
    * - insureds_company_has_respected_kfk_convention
      - bool
      - Insured company has respected KFK convention
    * - accident_occurred_in_belgium
      - bool
      - Did the accident occured in Belgium
    * - vehicles_have_mandatory_insurance
      - bool
      - Does the vehicles have mandatory insurance
    * - accident_meets_kfk_criteria
      - bool
      - Does the accident meets the KFK criteria
    * - access_locked
      - bool
      - Was the access locked
    * - rooms_occupied
      - bool
      - Where there rooms occupied
    * - threat_or_violence
      - bool
      - Were there threats or violence
    * - event_description
      - string
      - Description of the event
    * - description_of_circumstances
      - string
      - Description of the circumstances
    * - claim_advisor
      - Brokercloud User
      - Claims advisor
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the claim was opened or edited
    * - added_time
      - date Y-m-d H:i:s
      - The time the claim was created
    * - added_by
      - Brokercloud user
      - The user that created the party
    * - parties
      - array
      - Claim parties. Parties linked to the claim
    * - compensations
      - array
      - Claim compensations
    * - covers
      - array
      - Claim covers. Policies and guarantees covering the claim
    * - valuations
      - array
      - Claim valuations

Claim parties
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the claim_party
    * - party_id
      - int
      - Unique id of the party
    * - type
      - string
      - Type (Policy holder, Company, ...)
    * - claim_number_party
      - string
      - Claim number of the party
    * - policy_number_party
      - string
      - Policy number of the party
    * - claim_number_contact
      - string
      - Claim number of the primary contact of the party
    * - capacity_policy_holder
      - telebib
      - C384
    * - capacity_policy_holder
      - telebib
      - C384
    * - primary_contact_id
      - int
      - party_id of the primary contact
    * - link_policy_holder
      - telebib
      - CL91
    * - capacity_opposite_party
      - telebib
      - C114
    * - link_third_party
      - telebib
      - CL94
    * - type_of_expert
      - telebib
      - C416
    * - type_of_appointment_of_assessor
      - telebib
      - C413
    * - type_of_assessment
      - telebib
      - C411
    * - communication_text
      - string
      - Communication text

Claim compensations
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the claim_compensation
    * - accounting_date
      - date Y-m-d
      - Date booked in accounting
    * - period_start_date
      - date Y-m-d
      - Startdate period
    * - period_end_date
      - date Y-m-d
      - Enddate period
    * - assessment_number
      - string
      - Assessment number
    * - agent_account_number
      - string
      - Account number of the agent
    * - nature_of_payment
      - telebib
      - C60C
    * - type_of_payment
      - telebib
      - C60K
    * - means_of_payment
      - telebib
      - C608
    * - paying_party
      - telebib
      - C60N
    * - addressee_of_the_payment
      - telebib
      - C60D
    * - payment_done
      - bool
      - Has de payment been made
    * - paid_amount
      - float
      - Paid amount
    * - communication_text
      - string
      - Communication text
    * - beneficiary_id
      - int
      - party_id of the beneficiary
    * - recipient_id
      - int
      - party_id of the recipient
    * - type_of_beneficiary
      - telebib
      - C60A
    * - recipient_bic
      - string
      - BIC code of the recipient
    * - recipient_iban
      - string
      - IBAN code of the recipient
    * - recipient_agent_account_number
      - string
      - Account numer of the agent used by the recipient

Claim covers
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the cover
    * - policy_version_id
      - int
      - id of the policy endorsement
    * - guarantee_id
      - int
      - id of the guarantee
    * - cover
      - telebib
      - X026
    * - exemption
      - float
      - Exemption amount
    * - description
      - string
      - Description

Claim valuations
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the claim_party
    * - risk_version_id
      - int
      - id of the risk endorsement
    * - expedition_date
      - date Y-m-d
      - Shipping date expedition assignment
    * - reception_date_assessment_report
      - date Y-m-d
      - Date reception assessment report
    * - duration_of_deprivation_days
      - int
      - Duration of deprivation in days
    * - kfk_tariff
      - telebib
      - CP86
    * - origin_assessment_request
      - telebib
      - C418
    * - reason_for_the_assessment
      - telebib
      - C41Y
    * - type_of_mission_assessor
      - telebib
      - C41Z
    * - type_of_mission
      - telebib
      - C42F
    * - kfk
      - bool
      - Application of KFK
    * - total_loss
      - bool
      - Is total loss
    * - assessment_value
      - float
      - Value of assessment
    * - towing_costs
      - float
      - Towing costs
    * - deprivation_costs
      - float
      - Deprivation costs
    * - garaging_costs
      - float
      - Garaging costs
    * - description
      - string
      - Description

Address Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the address
    * - street 
      - string
      - Street
    * - number 
      - string
      - Number
    * - bus 
      - string
      - Bus
    * - extra
      - string
      - Extra
    * - city 
      - string
      - city
    * - postal_code 
      - string
      - Postal_code
    * - country 
      - string
      - Country    
    * - description
      - string
      - Description of the address

GET
-----

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - policy_version_id
      - int
      - id of the policy
    * - party_id
      - int
      - id of the party
    * - changed_since
      - unix timestamp
      - the claim changed since this date

**Headers**

The HTTP header X-OFFICE-ID is required for this endpoint.

**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "id":1,
    "office_id": 1,
    "claim_number":"201900001",
    "opening_date":"2019-01-03",
    "closure_date":null,
    "event_moment_time":"13:15:00",
    "event_moment_date":"2019-01-01",
    "notification_drawn_up_date":"2019-01-03",
    "type_of_management": {"code":"2", "description":"Maatschappij"},
    "state_of_file": {"code":"3", "description":"Lopende"},
    "liability": {"code":"8", "description":"Niet van toepassing"},
    "type_of_accident_liability":null,
    "liability_established":1,
    "assessor_is_appointed":2,
    "disbursement_agreement":0,
    "payment_request_received_from_third_party":0,
    "legal_action_against_opponent":0,
    "communication_text_insurer":null,
    "communication_text_insurer":null,
    "opinion_policy_holder_responsibility":null,
    "event_address": [{"id":1,"street":"Sint-Amandsesteenweg", "number":"92", "bus":"", "extra":"", "postal_code":"2880", "city":"Bornem", "country":"België", "description":"Our address"}],
    "police_report_address":null,
    "report_number":null,
    "report_date":null,
    "report_description":null,
    "claim_circumstances": {"code":"998", "description":"Ongekende omstandigheden"},
    "type_of_party_causing_the_accident":null,
    "relationship_policy_holder_and_party_causing_the_accident":null,
    "applied_sectoral_agreement":null,
    "origin_of_the_water_damage":null,
    "type_of_window_glass":null,
    "manner_of_burglary":null,
    "written_report":0,
    "witnesses":0,
    "dead_or_wounded":0,
    "the_declaration_mentions_wounded":0,
    "material_damage":0,
    "material_damages_other_than_to_vehicles":0,
    "immaterial_damages":0,
    "sectoral_agreement_applied":0,
    "application_kfk_convention":0,
    "our_insured_has_a_cl_guarantee":0,
    "insureds_company_has_respected_kfk_convention":0,
    "accident_occurred_in_belgium":0,
    "vehicles_have_mandatory_insurance":0,
    "accident_meets_kfk_criteria":0,
    "access_locked":0,
    "rooms_occupied":0,
    "threat_or_violence":0,
    "event_description":"Glasbraak",
    "description_of_circumstances":null,
    "claim_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "last_activity":"2019-01-10",
    "added_time":"2019-01-03",
    "added_by":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "parties":[{"id":1,type":{"code":61,"description":"Verzekeringsmaatschappij"},"party_id":1,"claim_number_party":"201900001","policy_number_party":"123456789","policy_number_contact":null,"claim_number_contact":null,"capacity_policy_holder":null,"primary_contact_id":null,"link_policy_holder":null,"capacity_opposite_party":null,"link_third_party":null,"type_of_expert":null,"type_of_appointment_of_assessor":null,"type_of_assessment":null,"communication_text":null},
    {"id":2,type":{"code":5,"description":"Verzekeringsnemer"},"party_id":1,"claim_number_party":"201900001","policy_number_party":"123456789","policy_number_contact":null,"claim_number_contact":null,"capacity_policy_holder":null,"primary_contact_id":null,"link_policy_holder":null,"capacity_opposite_party":null,"link_third_party":null,"type_of_expert":null,"type_of_appointment_of_assessor":null,"type_of_assessment":null,"communication_text":null}],
    "covers":[{"id":1,"party_id":1,"policy_version_id ":1,"guarantee_id":null,"cover":null,"claim_number_contact":null,"capacity_policy_holder":null,"primary_contact_id":null,"link_policy_holder":null}]
    "compensations": [],
    "valuations": []
  }


POST
------

With this API call it’s possible to create or update claims in Brokercloud. The idea is to be as complete as possible with the data for the party, but it’s already possible to create a claim with a minimum set of elements. One claim is created with one POST call, multiple claims will require multiple calls. When a claim is created in Brokercloud the API will response a “http 201 CREATED” message containing the the claim_id and all fields of the GET CLAIM in the body, when a claim is updated the response have the same body and will have a ‘http 200 OK’.

**Important notes:**

* To update and existing claim just include the id field (when this field is empty a new claim will automatically be created)
* The POST body has some object arrays such as phone covers and parties. Including an empty array in an update POST will remove the existing data, if you want to update only a single element in an object array we expect always to include the ones that should not be updated.
* As a general rule empty arrays will remove existing values in that array.


.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /claims



Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else don't provide field
      - 
      - Unique id of the claim
    * - claim_number
      - string
      - O
      - 
      - Claim number/broker reference
    * - opening_date
      - date Y-m-d
      - O
      - 
      - Opening date
    * - closure_date
      - date Y-m-d
      - O
      - 
      - Closure date
    * - event_moment_time
      - time H:i:s
      - O
      - 
      - time the event happened
    * - event_moment_date
      - date Y-m-d
      - O
      - 
      - date the event happened
    * - notification_drawn_up_date
      - date Y-m-d
      - O
      - 
      - Date drawn up
    * - type_of_management
      - string (telebib code)
      - O in case of update, else M
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C050
      - C050
    * - state_of_file
      - string (telebib code)
      - O in case of update, else M
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C051
      - C051
    * - liability
      - string (telebib code)
      - O in case of update, else M
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C250
      - C250
    * - type_of_accident_liability
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CNA1
      - CNA1
    * - liability_established
      - bool
      - O
      - 
      - Is the liability establised
    * - assessor_is_appointed
      - bool
      - O
      - 
      - Is the assessor appointed
    * - disbursement_agreement
      - bool
      - O
      - 
      - Is there an disbursement agreement
    * - payment_request_received_from_third_party
      - bool
      - O
      - 
      - Is there a received payment request from the third party
    * - legal_action_against_opponent
      - bool
      - O
      - 
      - Is there a legal action against the opponent
    * - communication_text_insurer
      - string
      - O
      - 
      - Communication text insurer
    * - opinion_policy_holder_responsibility
      - string
      - O
      - 
      - Opinion policyholder considering responsibility
    * - event_address
      - array
      - O
      - 
      - Address where the event happened
    * - police_report_address
      - array
      - O
      - 
      - Address on the police report
    * - report_number
      - string
      - O
      - 
      - Number of the police report
    * - report_date
      - date Y-m-d
      - O
      - 
      - Date of the police report
    * - report_description
      - string
      - O
      - 
      - Description on the police report
    * - claim_circumstances
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C221
      - C221
    * - type_of_party_causing_the_accident
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CNA2
      - CNA2
    * - relationship_policy_holder_and_party_causing_the_accident
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CNA5
      - CNA5
    * - applied_sectoral_agreement
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C059
      - C059
    * - origin_of_the_water_damage
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CM41
      - CM41
    * - type_of_window_glass
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CM51
      - CM51
    * - manner_of_burglary
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CM61
      - CM61
    * - written_report
      - bool
      - O
      - 
      - Is there a written report
    * - witnesses
      - bool
      - O
      - 
      - Are there witnesses
    * - dead_or_wounded
      - bool
      - O
      - 
      - Are there dead or wounded
    * - the_declaration_mentions_wounded
      - bool
      - O
      - 
      - Does the declaration mentions wounded
    * - material_damage
      - bool
      - O
      - 
      - Is there material damage
    * - material_damages_other_than_to_vehicles
      - bool
      - O
      - 
      - Are there material damages other than to vecicles
    * - immaterial_damages
      - bool
      - O
      - 
      - Are there immaterial damages
    * - sectoral_agreement_applied
      - bool
      - O
      - 
      - Is there a sectoral agreement applied
    * - application_kfk_convention
      - bool
      - O
      - 
      - Is there an application KFK convention
    * - our_insured_has_a_cl_guarantee
      - bool
      - O
      - 
      - Insured person has a CL guarantee
    * - insureds_company_has_respected_kfk_convention
      - bool
      - O
      - 
      - Insured company has respected KFK convention
    * - accident_occurred_in_belgium
      - bool
      - O
      - 
      - Did the accident occured in Belgium
    * - vehicles_have_mandatory_insurance
      - bool
      - O
      - 
      - Does the vehicles have mandatory insurance
    * - accident_meets_kfk_criteria
      - bool
      - O
      - 
      - Does the accident meets the KFK criteria
    * - access_locked
      - bool
      - O
      - 
      - Was the access locked
    * - rooms_occupied
      - bool
      - O
      - 
      - Where there rooms occupied
    * - threat_or_violence
      - bool
      - O
      - 
      - Were there threats or violence
    * - event_description
      - string
      - O
      - 
      - Description of the event
    * - description_of_circumstances
      - string
      - O
      - 
      - Description of the circumstances
    * - claim_advisor
      - Brokercloud User
      - O
      - 
      - Claims advisor
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the claim was opened or edited
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the claim was created
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the party
    * - parties
      - array
      - O
      - 
      - Claim parties. Parties linked to the claim
    * - compensations
      - array
      - O
      - 
      - Claim compensations
    * - covers
      - array
      - O
      - 
      - Claim covers. Policies and guarantees covering the claim
    * - valuations
      - array
      - O
      - 
      - Claim valuations

Claim parties
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - unique id of the claim_party
    * - party_id
      - int
      - M
      - 
      - Unique id of the party
    * - type
      - internal code
      - M
      - see list under POST parties section
      - Type (Policy holder, Company, ...)
    * - claim_number_party
      - string
      - O
      - 
      - Claim number of the party
    * - policy_number_party
      - string
      - O
      - 
      - Policy number of the party
    * - claim_number_contact
      - string
      - O
      - 
      - Claim number of the primary contact of the party
    * - capacity_policy_holder
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C384
      - C384
    * - primary_contact_id
      - int
      - O
      - 
      - party_id of the primary contact
    * - link_policy_holder
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CL91
      - CL91
    * - capacity_opposite_party
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C114
      - C114
    * - link_third_party
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CL94
      - CL94
    * - type_of_expert
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C416
      - C416
    * - type_of_appointment_of_assessor
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C413
      - C413
    * - type_of_assessment
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C411
      - C411
    * - communication_text
      - string
      - O
      - 
      - Communication text

Claim compensations
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - unique id of the claim_compensation
    * - accounting_date
      - date Y-m-d
      - O
      - 
      - Date booked in accounting
    * - period_start_date
      - date Y-m-d
      - O
      - 
      - Startdate period
    * - period_end_date
      - date Y-m-d
      - O
      - 
      - Enddate period
    * - assessment_number
      - string
      - O
      - 
      - Assessment number
    * - agent_account_number
      - string
      - O
      - 
      - Account number of the agent
    * - nature_of_payment
      - string (telebib code)
      - M
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C60C
      - C60C
    * - type_of_payment
      - string (telebib code)
      - M
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C60K
      - C60K
    * - means_of_payment
      - string (telebib code)
      - M
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C608
      - C608
    * - paying_party
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C60N
      - C60N
    * - addressee_of_the_payment
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C60D
      - C60D
    * - payment_done
      - bool
      - O
      - 
      - Has de payment been made
    * - paid_amount
      - float
      - O
      - 
      - Paid amount
    * - communication_text
      - string
      - M
      - 
      - Communication text
    * - beneficiary_id
      - int
      - O
      - 
      - party_id of the beneficiary
    * - recipient_id
      - int
      - O
      - 
      - party_id of the recipient
    * - type_of_beneficiary
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C60A
      - C60A
    * - recipient_bic
      - string
      - O
      - 
      - BIC code of the recipient
    * - recipient_iban
      - string
      - O
      - 
      - IBAN code of the recipient
    * - recipient_agent_account_number
      - string
      - O
      - 
      - Account numer of the agent used by the recipient

Claim covers
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - unique id of the cover
    * - policy_version_id
      - int
      - M
      - 
      - id of the policy endorsement
    * - guarantee_id
      - int
      - M
      - 
      - id of the guarantee
    * - cover
      - string (telebib code)
      - O
      - https://www.telebib2.org//3posQLists.asp?XList=X026
      - X026
    * - exemption
      - float
      - O
      - 
      - Exemption amount
    * - description
      - string
      - O
      - 
      - Description

Claim valuations
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - unique id of the claim_party
    * - risk_version_id
      - int
      - O
      - 
      - id of the risk endorsement
    * - expedition_date
      - date Y-m-d
      - O
      - 
      - Shipping date expedition assignment
    * - reception_date_assessment_report
      - date Y-m-d
      - O
      - 
      - Date reception assessment report
    * - duration_of_deprivation_days
      - int
      - O
      - 
      - Duration of deprivation in days
    * - kfk_tariff
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CP86
      - CP86
    * - origin_assessment_request
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C418
      - C418
    * - reason_for_the_assessment
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C41Y
      - C41Y
    * - type_of_mission_assessor
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C41Z
      - C41Z
    * - type_of_mission
      - string (telebib code)
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=C42F
      - C42F
    * - kfk
      - bool
      - O
      - 
      - Application of KFK
    * - total_loss
      - bool
      - O
      - 
      - Is total loss
    * - assessment_value
      - float
      - O
      - 
      - Value of assessment
    * - towing_costs
      - float
      - O
      - 
      - Towing costs
    * - deprivation_costs
      - float
      - O
      - 
      - Deprivation costs
    * - garaging_costs
      - float
      - O
      - 
      - Garaging costs
    * - description
      - string
      - O
      - 
      - Description

Address Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandaotry/optional
      - Possible values
      - Description
    * - id
      - int
      - O
      - 
      - Unique id of the address
    * - street 
      - string
      - O
      - 
      - Street
    * - number 
      - string
      - O
      - 
      - Number
    * - bus 
      - string
      - O
      - 
      - Bus
    * - extra
      - string
      - O
      - 
      - Extra
    * - city 
      - string
      - O
      - 
      - city
    * - postal_code 
      - string
      - O
      - 
      - Postal_code
    * - country 
      - string
      - O
      - 
      - Country    
    * - description
      - string
      - O
      - 
      - Description of the address

**Example request**:

.. sourcecode:: guess
  
  POST /claims HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg
        
  {
    "claim_number":"201900001",
    "opening_date":"2019-01-03",
    "event_moment_time":"13:15:00",
    "event_moment_date":"2019-01-01",
    "notification_drawn_up_date":"2019-01-03",
    "type_of_management": "2",
    "state_of_file": "3",
    "liability": "8",
    "liability_established": true,
    "assessor_is_appointed": false,
    "disbursement_agreement": false,
    "payment_request_received_from_third_party": false,
    "legal_action_against_opponent": false,
    "event_address": [{"street":"Sint-Amandsesteenweg", "number":"92", "postal_code":"2880", "city":"Bornem", "country":"België", "description":"Our address"}],
    "claim_circumstances": "998",
    "written_report": false,
    "witnesses": false,
    "dead_or_wounded": false,
    "the_declaration_mentions_wounded": false,
    "material_damage": false,
    "material_damages_other_than_to_vehicles": false,
    "immaterial_damages": false,
    "sectoral_agreement_applied": false,
    "application_kfk_convention": false,
    "our_insured_has_a_cl_guarantee": false,
    "insureds_company_has_respected_kfk_convention": false,
    "accident_occurred_in_belgium": false,
    "vehicles_have_mandatory_insurance": false,
    "accident_meets_kfk_criteria": false,
    "access_locked": false,
    "rooms_occupied": false,
    "threat_or_violence": false,
    "event_description":"Glasbraak",
    "claim_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "last_activity":"2019-01-10",
    "added_time":"2019-01-03",
    "added_by":{"lastname":"Hinckxt", "firstname":"Lothar"},
    "parties":[{"type": "61", "party_id":1, "claim_number_party":"201900001", "policy_number_party":"123456789"},
    {"type": "5", "party_id":1, "claim_number_party":"201900001", "policy_number_party":"123456789"}],
    "covers":{"party_id":1,"policy_version_id ":1,"guarantee_id":5}
  }



**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "id":1,
    "office_id": 1,
    "claim_number":"201900001",
    "opening_date":"2019-01-03",
    "closure_date":null,
    "event_moment_time":"13:15:00",
    "event_moment_date":"2019-01-01",
    "notification_drawn_up_date":"2019-01-03",
    "type_of_management": {"code":"2", "description":"Maatschappij"},
    "state_of_file": {"code":"3", "description":"Lopende"},
    "liability": {"code":"8", "description":"Niet van toepassing"},
    "type_of_accident_liability":null,
    "liability_established":1,
    "assessor_is_appointed":2,
    "disbursement_agreement":0,
    "payment_request_received_from_third_party":0,
    "legal_action_against_opponent":0,
    "communication_text_insurer":null,
    "communication_text_insurer":null,
    "opinion_policy_holder_responsibility":null,
    "event_address": [{"id":1,"street":"Sint-Amandsesteenweg", "number":"92", "bus":"", "extra":"", "postal_code":"2880", "city":"Bornem", "country":"België", "description":"Our address"}],
    "police_report_address":null,
    "report_number":null,
    "report_date":null,
    "report_description":null,
    "claim_circumstances": {"code":"998", "description":"Ongekende omstandigheden"},
    "type_of_party_causing_the_accident":null,
    "relationship_policy_holder_and_party_causing_the_accident":null,
    "applied_sectoral_agreement":null,
    "origin_of_the_water_damage":null,
    "type_of_window_glass":null,
    "manner_of_burglary":null,
    "written_report":0,
    "witnesses":0,
    "dead_or_wounded":0,
    "the_declaration_mentions_wounded":0,
    "material_damage":0,
    "material_damages_other_than_to_vehicles":0,
    "immaterial_damages":0,
    "sectoral_agreement_applied":0,
    "application_kfk_convention":0,
    "our_insured_has_a_cl_guarantee":0,
    "insureds_company_has_respected_kfk_convention":0,
    "accident_occurred_in_belgium":0,
    "vehicles_have_mandatory_insurance":0,
    "accident_meets_kfk_criteria":0,
    "access_locked":0,
    "rooms_occupied":0,
    "threat_or_violence":0,
    "event_description":"Glasbraak",
    "description_of_circumstances":null,
    "claim_advisor":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "last_activity":"2019-01-10",
    "added_time":"2019-01-03",
    "added_by":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "parties":[{"id":1,"type":{"code":61,"description":"Verzekeringsmaatschappij"},"party_id":1,"claim_number_party":"201900001","policy_number_party":"123456789","policy_number_contact":null,"claim_number_contact":null,"capacity_policy_holder":null,"primary_contact_id":null,"link_policy_holder":null,"capacity_opposite_party":null,"link_third_party":null,"type_of_expert":null,"type_of_appointment_of_assessor":null,"type_of_assessment":null,"communication_text":null},
    {"id":2,"type":{"code":5,"description":"Verzekeringsnemer"},"party_id":1,"claim_number_party":"201900001","policy_number_party":"123456789","policy_number_contact":null,"claim_number_contact":null,"capacity_policy_holder":null,"primary_contact_id":null,"link_policy_holder":null,"capacity_opposite_party":null,"link_third_party":null,"type_of_expert":null,"type_of_appointment_of_assessor":null,"type_of_assessment":null,"communication_text":null}],
    "covers":[{"id":1,"party_id":1,"policy_version_id ":1,"guarantee_id":null,"cover":null,"claim_number_contact":null,"capacity_policy_holder":null,"primary_contact_id":null,"link_policy_holder":null}]
    "compensations": [],
    "valuations": []
  }


Risks
==========

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /risks/{id}


Risk Aircraft Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '009'
      - telebib
      - X052 (value = '009')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)      
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - make
      - string
      - Make of the aircraft
    * - model
      - string
      - Model of the aircraft
    * - type
      - string
      - Type of the aircraft
    * - identification_mark
      - string
      - Identification mark
    * - chassisnumber
      - string
      - Chassisnumber
    * - aircraft_type
      - telebib
      - 5900
    * - drone_usage_category
      - telebib
      - 5910
    * - mtom
      - float
      - Maximum take-off mass

Risk Animal Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '090'
      - telebib
      - X052 (value = '090')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - Name of the animal
    * - purchase_date
      - date Y-m-d
      - Purchase date
    * - registration_number
      - string
      - Registration number
    * - animal_type
      - telebib
      - 4132
    * - sex
      - telebib
      - A124
    * - number_of_animals
      - int
      - Number of animals
    * - animal_description
      - string
      - Animal description

Risk Association Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '070'
      - telebib
      - X052 (value = '070')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - address
      - array
      - Address of the association. Address Fields
    * - name
      - string
      - Name of the association
    * - company_number
      - string
      - Company number
    * - establishment_number    
      - string
      - Establishment number
    * - lei
      - string
      - Legal entity identifier
    * - business_code_2008
      - int
      - Nacebel number
    * - number_insured
      - int
      - Number insured
    * - group_activity_description
      - string
      - Group activity description

Risk Building Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '010' | '011'
      - telebib
      - X052 (value = '010' for Building | value = '011' for Contents)
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - address
      - array
      - Address of the building. Address Fields
    * - production_year
      - int
      - Production year
    * - temporary_delivery_date
      - date Y-m-d
      - Date temporary delivery
    * - building_use
      - telebib
      - 3100
    * - construction_usage
      - telebib
      - 3172
    * - construction_nature
      - telebib
      - 3173
    * - business_code_2008
      - int
      - Nacebel code
    * - policy_holder_category
      - telebib
      - 3110
    * - type_calculation_table
      - telebib
      - 3151
    * - dwelling_type
      - telebib
      - 3170
    * - dwelling_type_detail
      - telebib
      - 3171
    * - residence_type
      - telebib
      - 3020
    * - construction_type
      - telebib
      - 3V12
    * - prefab_type
      - telebib
      - 3V1C
    * - roof_type
      - telebib
      - 3V16
    * - roof_type_detail
      - telebib
      - 3V1H
    * - contiguity
      - telebib
      - 3V62
    * - alarm_recognition1
      - telebib
      - 3462
    * - alarm_recognition2
      - telebib
      - 3462
    * - alarm_recognition3
      - telebib
      - 3462
    * - alarm_recognition4
      - telebib
      - 3462
    * - alarm_recognition5
      - telebib
      - 3462
    * - alarm_recognition6
      - telebib
      - 3462
    * - occupation_degree
      - telebib
      - 3V61
    * - appeal_dropped_type
      - telebib
      - M03J
    * - building_under_construction
      - bool
      - Is the building under construction
    * - prefabricated_dwelling
      - bool
      - Is the building prefabricated
    * - building_equivalent_to_new_construction
      - bool
      - Is the building equivalent to a new construction
    * - adjoining_building
      - bool
      - Is there an adjoining building
    * - building_with_several_apartments
      - bool
      - Does the building have several apartments
    * - presence_of_a_neon_light
      - bool
      - Is there a neon light
    * - building_with_combustible_materials
      - bool
      - Does the building have combustible materials
    * - elements_with_combustible_materials
      - bool
      - Does the building have load-bearing elements in combustible materials
    * - presence_burglaryprotection_system
      - bool
      - Is there a burglary protection system
    * - presence_fireprotection_system
      - bool
      - Is there a fire protection system
    * - presence_antenna
      - bool
      - Is there an antenna
    * - platform_combustible_materials
      - bool
      - Is there a platform in combustible materials
    * - vehicle_included
      - bool
      - Is there a vehicle included in the risk
    * - building_live_in_concierge
      - bool
      - Is it a building with resident caretaker
    * - appartment_above_groundlevel
      - bool
      - Is there an appartment located on a higher level than the ground floor
    * - building_cellars_annexes_garages
      - bool
      - is it a building with cellars, outbuildings or garages
    * - neon_sign_value
      - float
      - Value signboard
    * - window_glass_value
      - float
      - Value windows
    * - calculation_table_result
      - float
      - Result calculation table
    * - value_per_object
      - float
      - Value per object
    * - number_apartments
      - int
      - Number apartments
    * - number_floors
      - int
      - Number of floors
    * - number_square_metres_signs
      - int
      - Number of square meters signboard
    * - number_places
      - int
      - Number of places
    * - number_lifts
      - int
      - Number of lifts
    * - number_hoists
      - int
      - Number of lifting devices
    * - glass_roof
      - int
      - Surface glass roof
    * - surface_area
      - int
      - Surface area
    * - distance_between_buildings
      - int
      - Distance between buildings
    * - maximal_annual_number_of_nights_absent
      - int
      - Maximum number of consecutive nights absence
    * - number_of_subsequent_nights_uninhabited
      - int
      - Maximum number of nights of absence per year
    * - percentage_combustible_materials
      - float
      - Percentage of combustible materials
    * - capital
      - float
      - Capital
    * - abex_index
      - int
      - Abex index of subscription

Risk Family Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '060'
      - telebib
      - X052 (value = '060')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - Name
    * - address
      - array
      - Address of the family. Address Fields
    * - cover_type
      - telebib
      - 4100
    * - family_type
      - telebib
      - 4160
    * - number_family_members
      - int
      - Number of people in the family
    * - number_children
      - int
      - Number of children

Risk Group Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '04*'
      - telebib
      - X052 (value = '040', '042', ...)
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - Name
    * - address
      - array
      - Address of the group. Address Fields
    * - plate_number
      - string
      - License plate
    * - business_code_2008
      - int
      - Nacebel code
    * - blue_collar_risk_classification
      - telebib
      - 641C
    * - white_collar_risk_classification
      - telebib
      - 641D
    * - non_subjects
      - bool
      - Non submitted (workers compensation)
    * - total_salary_upto_legal_maximum
      - float
      - Total salary limited to the legal maximum
    * - total_salary_exceding_legal_maximum
      - float
      - Total salary that exceeds the legal maximum
    * - number_insured
      - int
      - Number of insured persons in the group
    * - number_employees
      - int
      - Number of employees
    * - surface_area
      - float
      - Surface area
    * - number_vehicles
      - int
      - Number of vehicles
    * - number_goods_rented
      - int
      - Number of goods in rent

  Risk Group_activity Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '05*'
      - telebib
      - X052 (value = '050', '051', ...)
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - address
      - array
      - Address of the groupactivity. Address Fields
    * - name
      - string
      - Name
    * - company_number
      - string
      - Company number
    * - establishment_number    
      - string
      - Establishment number
    * - lei
      - string
      - Legal entity identifier
    * - business_code_2008
      - int
      - Nacebel number
    * - number_insured
      - int
      - Number insured
    * - group_activity_description
      - string
      - Group activity description  

Risk Motorvehicle Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '001'
      - telebib
      - X052 (value = '001')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - make
      - string
      - Make of the vehicle
    * - model
      - string
      - Model of the vehicle
    * - type
      - string
      - Type of the vehicle
    * - plate_number
      - string
      - License plate
    * - chassis_number
      - string
      - Chassis number
    * - check_digit
      - string
      - Check digit
    * - vin_unifier
      - string
      - VIN unifier
    * - pvg_number
      - string
      - PVA number
    * - european_pvg_number
      - string
      - European PVA number
    * - european_name
      - string
      - European name vehicle
    * - div_code
      - string
      - VRO (VehicleRegistration) code (make - model)
    * - wvta_code_number
      - string
      - WVTA-code Number (Whole Vehicle Type Approval)
    * - wvta_code_variant
      - string
      - WVTA-code variant (Whole Vehicle Type Approval)
    * - wvta_code_version
      - string
      - WVTA-code version (Whole Vehicle Type Approval)
    * - adr_code
      - string
      - ADR code (from French title: Accord européen relatif au transport international des marchandises Dangereuses par Route -> Dangerous Goods Transport)
    * - first_use_date
      - date Y-m-d
      - Date of first use
    * - production_year
      - int
      - Year of production
    * - registration_date
      - date Y-m-d
      - Registration date
    * - plate_number_withdrawal_date
      - date Y-m-d
      - License plate withdrawal date
    * - type_div
      - telebib
      - 5003
    * - vehicle_usage
      - telebib
      - 5200
    * - property_type
      - telebib
      - 5609
    * - fuel_type
      - telebib
      - 5015
    * - traction_type
      - telebib
      - 501G
    * - garage
      - telebib
      - 5420
    * - special_license_plate_type
      - telebib
      - 500J
    * - vehicle_category
      - telebib
      - 500G
    * - license_plate_format
      - telebib
      - 5023
    * - color
      - telebib
      - 5019
    * - anti_theft_system1
      - telebib
      - 5634
    * - anti_theft_system2
      - telebib
      - 5634
    * - anti_theft_system3
      - telebib
      - 5634
    * - pedaling_assistance
      - telebib
      - 5026
    * - financing
      - bool
      - Financing?
    * - coverage_tax
      - bool
      - Tax for putting into circulation covered?
    * - tax
      - float
      - Tax on putting into circulation
    * - list_value
      - float
      - List value
    * - list_value_vat_included
      - float
      - Listed value (VAT incl.)
    * - power
      - int
      - Power
    * - engine_capacity
      - float
      - Engine capacity
    * - number_of_seats
      - int
      - Number of seats
    * - number_of_doors
      - int
      - Number of doors
    * - maximum_authorised_weight
      - float
      - Maximum authorised weight
    * - tare
      - float
      - Tare (weight of an empty vehicle)
    * - weight_in_working_order
      - float
      - Operating weight
    * - maximum_weight_of_the_running_gear
      - float
      - Maximum weight of the running gear
    * - places
      - int
      - Places
    * - percentage_of_vat_exemption      
      - float
      - Percentage of VAT exemption
    * - co2
      - float
      - CO2
    * - km
      - int
      - Mileage
    * - km_date
      - date Y-m-d
      - Date of recording mileage
    * - max_km_yearly
      - int
      - Maximum number of kilometers / year
    * - div_conform
      - bool
      - VRO identifiers unchanged
    * - convertible
      - bool
      - Is convertible?
    * - coupe    
      - bool
      - Is coupé
    * - fleet
      - bool
      - Vehicle belongs to fleet
    * - sportscar
      - bool
      - Is sports car
    * - suv
      - bool
      - Is SUV
    * - anti_theft_system
      - bool
      - Has anti theft system
    * - international_transports
      - bool
      - International transport?
    * - speed_pedelec
      - bool
      - Vehicle is a Speed pedelec?

Risk Person Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '030'
      - telebib
      - X052 (value = '030')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - party_id
      - int
      - id of the party insured in the risk_person.

Risk Trailer Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '002'
      - telebib
      - X052 (value = '002')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - make
      - string
      - Make of the trailer
    * - model
      - string
      - Model of the trailer
    * - type
      - string
      - Type of the trailer
    * - plate_number
      - string
      - License plate
    * - chassis_number
      - string
      - Chassis number
    * - check_digit
      - string
      - Check digit
    * - vin_unifier
      - string
      - VIN unifier
    * - pvg_number
      - string
      - PVA number
    * - european_pvg_number
      - string
      - European PVA number
    * - european_name
      - string
      - European name trailer
    * - div_code
      - string
      - VRO code (make - model)
    * - wvta_code_number
      - string
      - WVTA-code Number (Whole Vehicle Type Approval)
    * - wvta_code_variant
      - string
      - WVTA-code variant (Whole Vehicle Type Approval)
    * - wvta_code_version
      - string
      - WVTA-code version (Whole Vehicle Type Approval)
    * - first_use_date
      - date Y-m-d
      - Date of first use
    * - production_year
      - int
      - Year of production
    * - registration_date
      - date Y-m-d
      - Registration date
    * - plate_number_withdrawal_date
      - date Y-m-d
      - License plate withdrawal date
    * - type_div
      - telebib
      - 5003
    * - special_license_plate_type
      - telebib
      - 500J
    * - vehicle_category
      - telebib
      - 500G
    * - license_plate_format
      - telebib
      - 5023
    * - coverage_tax
      - bool
      - Tax for putting into circulation covered?
    * - tax
      - float
      - Tax on putting into circulation
    * - list_value
      - float
      - List value
    * - list_value_vat_included
      - float
      - Listed value (VAT incl.)
    * - maximum_authorised_weight
      - float
      - Maximum authorised weight
    * - tare
      - float
      - Tare (weight of an empty vehicle)
    * - weight_in_working_order
      - float
      - Operating weight
    * - maximum_weight_of_the_running_gear
      - float
      - Maximum weight of the running gear
    * - percentage_of_vat_exemption
      - float
      - Percentage of VAT exemption
    * - div_conform
      - bool
      - VRO identifiers unchanged
    * - international_transports
      - bool
      - International transport?

Risk Vessel Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type = '003'
      - telebib
      - X052 (value = '003')
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - Name of the vessel
    * - make
      - string
      - Make of the vessel
    * - model
      - string
      - Model of the vessel
    * - type
      - string
      - Type of the vessel
    * - production_year
      - int
      - Year of production
    * - address
      - array
      - Address where the vessel was constructed. Address Fields
    * - hullidentification
      - string
      - Hull identification number
    * - certificate_colours_code
      - string
      - Certificate of colours - Code
    * - certificate_of_colours_number
      - string
      - Certificate of colours - Number
    * - plate_number
      - string
      - License plate
    * - vessel_usage
      - telebib
      - 5800
    * - vessel_type
      - telebib
      - 5801
    * - rigging
      - telebib
      - 5802
    * - constructionmaterials
      - telebib
      - 5803
    * - total_length
      - float
      - Total length
    * - length
      - float
      - Length
    * - width
      - float
      - Width
    * - draught
      - float
      - Draught
    * - port_of_register
      - string
      - Home port
    * - shipyard
      - string
      - Shipyard

Risk Other Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - risk_type
      - telebib
      - X052 (value = all telebib codes not used in the above risk types)
    * - id
      - int
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - unique id of the risk.    
    * - description
      - string
      - Description of the risk
    * - current_endorsement
      - bool
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - The time the risk was created
    * - parties
      - array
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - Name
    * - other_description
      - string
      - Description

Address Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the address
    * - street 
      - string
      - Street
    * - number 
      - string
      - Number
    * - bus 
      - string
      - Bus
    * - extra
      - string
      - Extra
    * - city 
      - string
      - city
    * - postal_code 
      - string
      - Postal_code
    * - country 
      - string
      - Country    
    * - description
      - string
      - Description of the address

Risk parties
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the risk_party
    * - risk_version_id
      - int
      - id of the risk endorsement
    * - party_id
      - int
      - Unique id of the party    
    * - type
      - string
      - Type (Owner, Driver, Insured, ...)
    * - driver_type
      - telebib
      - 5125

GET
-----

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - policy_version_id
      - int
      - id of the policy version
    * - policy_id
      - int
      - id of the policy
    * - risk_id
      - int
      - id of the risk
    * - party_id
      - int
      - id of the party
    * - current_endorsement
      - int
      - risk is the current endorsement
    * - changed_since
      - unix timestamp
      - the risk changed since this date

**Headers**

The HTTP header X-OFFICE-ID is required for this endpoint.

**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "risk_type": {"code":"001", "description":"Motorvoertuig"},
    "id":1,
    "risk_id":1,
    "description":"1-ABC-123 Mercedes cla 180d",
    "current_endorsement":1,
    "last_activity":"2019-01-01 10:25:25",
    "added_by":{"id":1, "lastname":"Hinckxt", "firstname":"Lothar"},
    "added_time":"2019-01-01",
    "make":"Mercedes",
    "model":"cla",
    "type":"180d",
    "plate_number":"1-ABC-123",
    "chassis_number":"WVWZZZAUZZZ000001",
    "check_digit":null,
    "vin_unifier":null,
    "pvg_number":null,
    "european_pvg_number":null,
    "european_name":null,
    "div_code":null,
    "wvta_code_number":null,
    "wvta_code_variant":null,
    "wvta_code_version":null,
    "adr_code":null,
    "first_use_date":"2019-01-01",
    "production_year":2018,
    "registration_date":null,
    "plate_number_withdrawal_date":null,
    "type_div": {"code":"VP", "description":"Personenauto (*)"},
    "vehicle_usage": {"code":"12", "description":"Privégebruik met woon-werkverkeer"},
    "property_type": {"code":"1", "description":"Volle eigendom"},
    "fuel_type" {"code":"2", "description":"Diesel"},
    "traction_type":null,
    "garage":null,
    "special_license_plate_type":null,
    "vehicle_category":null,
    "license_plate_format":null,
    "color":null,
    "anti_theft_system1":null,
    "anti_theft_system2":null,
    "anti_theft_system3":null,
    "pedaling_assistance":null,
    "financing":0,
    "coverage_tax":0,
    "tax":null,
    "list_value":null,
    "list_value_vat_included":null,
    "power":null,
    "engine_capacity":null,
    "number_of_seats":5,
    "number_of_doors":5,
    "maximum_authorised_weight":null,
    "tare":null,
    "weight_in_working_order":null,
    "maximum_weight_of_the_running_gear":null,
    "places":null,
    "percentage_of_vat_exemption":null,
    "co2":null,
    "km":null,
    "km_date":null,
    "max_km_yearly":null,
    "div_conform":null,
    "convertible":null,
    "coupe":null,
    "fleet":null,
    "sportscar":null,
    "suv":null,
    "anti_theft_system":null,
    "international_transports":null,
    "speed_pedelec":null,
    "parties":[{"id":1,"type":{"code":170,"description":"Bestuurder"},"party_id":1,"driver_type":{"code":"1","description":"Gebruikelijke"}}]
  }

POST risk (in progress)
--------------


.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /risks


Risk Aircraft Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '009'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '009')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, else O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)      
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - N/A
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - make
      - string
      - O
      - 
      - Make of the aircraft
    * - model
      - string
      - O
      - 
      - Model of the aircraft
    * - type
      - string
      - O
      - 
      - Type of the aircraft
    * - identification_mark
      - string
      - O
      - 
      - Identification mark
    * - chassisnumber
      - string
      - O
      - 
      - Chassisnumber
    * - aircraft_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5900
      - 5900
    * - drone_usage_category
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5910
      - 5910
    * - mtom
      - float
      - O
      - 
      - Maximum take-off mass

Risk Animal Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '090'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '090')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, elso O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - O
      - 
      - Name of the animal
    * - purchase_date
      - date Y-m-d
      - O
      - 
      - Purchase date
    * - registration_number
      - string
      - O
      - 
      - Registration number
    * - animal_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=4132
      - 4132
    * - sex
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=A124
      - A124
    * - number_of_animals
      - int
      - O
      - 
      - Number of animals
    * - animal_description
      - string
      - O
      - 
      - Animal description

Risk Association Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '070'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '070')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, elso O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - address
      - array
      - O
      - 
      - Address of the association. Address Fields
    * - name
      - string
      - O
      - 
      - Name of the association
    * - company_number
      - string
      - O
      - 
      - Company number
    * - establishment_number    
      - string
      - O
      - 
      - Establishment number
    * - lei
      - string
      - O
      - 
      - Legal entity identifier
    * - business_code_2008
      - int
      - O
      - 
      - Nacebel number
    * - number_insured
      - int
      - O
      - 
      - Number insured
    * - group_activity_description
      - string
      - O
      - 
      - Group activity description

Risk Building Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '010' | '011'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '010' for Building | value = '011' for Contents)
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, else O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - address
      - array
      - O
      - 
      - Address of the building. Address Fields
    * - production_year
      - int
      - O
      - 
      - Production year
    * - temporary_delivery_date
      - date Y-m-d
      - O
      - 
      - Date temporary delivery
    * - building_use
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3100
      - 3100
    * - construction_usage
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3172
      - 3172
    * - construction_nature
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3173
      - 3173
    * - business_code_2008
      - int
      - O
      - 
      - Nacebel code
    * - policy_holder_category
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3110
      - 3110
    * - type_calculation_table
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3151
      - 3151
    * - dwelling_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3170
      - 3170
    * - dwelling_type_detail
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=6171
      - 3171
    * - residence_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3020
      - 3020
    * - construction_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3V12
      - 3V12
    * - prefab_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3V1C
      - 3V1C
    * - roof_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3V16
      - 3V16
    * - roof_type_detail
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3V1H
      - 3V1H
    * - contiguity
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3V62
      - 3V62
    * - alarm_recognition1
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3462
      - 3462
    * - alarm_recognition2
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3462
      - 3462
    * - alarm_recognition3
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3462
      - 3462
    * - alarm_recognition4
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3462
      - 3462
    * - alarm_recognition5
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3462
      - 3462
    * - alarm_recognition6
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3462
      - 3462
    * - occupation_degree
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3V61
      - 3V61
    * - appeal_dropped_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=M03J
      - M03J
    * - building_under_construction
      - bool
      - O
      - 
      - Is the building under construction
    * - prefabricated_dwelling
      - bool
      - O
      - 
      - Is the building prefabricated
    * - building_equivalent_to_new_construction
      - bool
      - O
      - 
      - Is the building equivalent to a new construction
    * - adjoining_building
      - bool
      - O
      - 
      - Is there an adjoining building
    * - building_with_several_apartments
      - bool
      - O
      - 
      - Does the building have several apartments
    * - presence_of_a_neon_light
      - bool
      - O
      - 
      - Is there a neon light
    * - building_with_combustible_materials
      - bool
      - O
      - 
      - Does the building have combustible materials
    * - elements_with_combustible_materials
      - bool
      - O
      - 
      - Does the building have load-bearing elements in combustible materials
    * - presence_burglaryprotection_system
      - bool
      - O
      - 
      - Is there a burglary protection system
    * - presence_fireprotection_system
      - bool
      - O
      - 
      - Is there a fire protection system
    * - presence_antenna
      - bool
      - O
      - 
      - Is there an antenna
    * - platform_combustible_materials
      - bool
      - O
      - 
      - Is there a platform in combustible materials
    * - vehicle_included
      - bool
      - O
      - 
      - Is there a vehicle included in the risk
    * - building_live_in_concierge
      - bool
      - O
      - 
      - Is it a building with resident caretaker
    * - appartment_above_groundlevel
      - bool
      - O
      - 
      - Is there an appartment located on a higher level than the ground floor
    * - building_cellars_annexes_garages
      - bool
      - O
      - 
      - is it a building with cellars, outbuildings or garages
    * - neon_sign_value
      - float
      - O
      - 
      - Value signboard
    * - window_glass_value
      - float
      - O
      - 
      - Value windows
    * - calculation_table_result
      - float
      - O
      - 
      - Result calculation table
    * - value_per_object
      - float
      - O
      - 
      - Value per object
    * - number_apartments
      - int
      - O
      - 
      - Number apartments
    * - number_floors
      - int
      - O
      - 
      - Number of floors
    * - number_square_metres_signs
      - int
      - O
      - 
      - Number of square meters signboard
    * - number_places
      - int
      - O
      - 
      - Number of places
    * - number_lifts
      - int
      - O
      - 
      - Number of lifts
    * - number_hoists
      - int
      - O
      - 
      - Number of lifting devices
    * - glass_roof
      - int
      - O
      - 
      - Surface glass roof
    * - surface_area
      - int
      - O
      - 
      - Surface area
    * - distance_between_buildings
      - int
      - O
      - 
      - Distance between buildings
    * - maximal_annual_number_of_nights_absent
      - int
      - O
      - 
      - Maximum number of consecutive nights absence
    * - number_of_subsequent_nights_uninhabited
      - int
      - O
      - 
      - Maximum number of nights of absence per year
    * - percentage_combustible_materials
      - float
      - O
      - 
      - Percentage of combustible materials
    * - capital
      - float
      - O
      - 
      - Capital
    * - abex_index
      - int
      - O
      - 
      - Abex index of subscription

Risk Family Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '060'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '060')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of udpate, elso O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - O
      - 
      - Name
    * - address
      - array
      - O
      - 
      - Address of the family. Address Fields
    * - cover_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=4100
      - 4100
    * - family_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=4160
      - 4160
    * - number_family_members
      - int
      - O
      - 
      - Number of people in the family
    * - number_children
      - int
      - O
      - 
      - Number of children

Risk Group Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '04*'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '040', '042', ...)
    * - id
      - int
      - O
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - O
      - 
      - Name
    * - address
      - array
      - O
      - 
      - Address of the group. Address Fields
    * - plate_number
      - string
      - O
      - 
      - License plate
    * - business_code_2008
      - int
      - O
      - 
      - Nacebel code
    * - blue_collar_risk_classification
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=641C
      - 641C
    * - white_collar_risk_classification
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=641D
      - 641D
    * - non_subjects
      - bool
      - O
      - 
      - Non submitted (workers compensation)
    * - total_salary_upto_legal_maximum
      - float
      - O
      - 
      - Total salary limited to the legal maximum
    * - total_salary_exceding_legal_maximum
      - float
      - O
      - 
      - Total salary that exceeds the legal maximum
    * - number_insured
      - int
      - O
      - 
      - Number of insured persons in the group
    * - number_employees
      - int
      - O
      - 
      - Number of employees
    * - surface_area
      - float
      - O
      - 
      - Surface area
    * - number_vehicles
      - int
      - O
      - 
      - Number of vehicles
    * - number_goods_rented
      - int
      - O
      - 
      - Number of goods in rent

  Risk Group_activity Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '05*'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '050', '051', ...)
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, else O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - address
      - array
      - O
      - 
      - Address of the groupactivity. Address Fields
    * - name
      - string
      - O
      - 
      - Name
    * - company_number
      - string
      - O
      - 
      - Company number
    * - establishment_number    
      - string
      - O
      - 
      - Establishment number
    * - lei
      - string
      - O
      - 
      - Legal entity identifier
    * - business_code_2008
      - int
      - O
      - 
      - Nacebel number
    * - number_insured
      - int
      - O
      - 
      - Number insured
    * - group_activity_description
      - string
      - O
      - 
      - Group activity description  

Risk Motorvehicle Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '001'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '001')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, else O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - make
      - string
      - O
      - 
      - Make of the vehicle
    * - model
      - string
      - O
      - 
      - Model of the vehicle
    * - type
      - string
      - O
      - 
      - Type of the vehicle
    * - plate_number
      - string
      - O
      - 
      - License plate
    * - chassis_number
      - string
      - O
      - 
      - Chassis number
    * - check_digit
      - string
      - O
      - 
      - Check digit
    * - vin_unifier
      - string
      - O
      - 
      - VIN unifier
    * - pvg_number
      - string
      - O
      - 
      - PVA number
    * - european_pvg_number
      - string
      - O
      - 
      - European PVA number
    * - european_name
      - string
      - O
      - 
      - European name vehicle
    * - div_code
      - string
      - O
      - 
      - VRO (VehicleRegistration) code (make - model)
    * - wvta_code_number
      - string
      - O
      - 
      - WVTA-code Number (Whole Vehicle Type Approval)
    * - wvta_code_variant
      - string
      - O
      - 
      - WVTA-code variant (Whole Vehicle Type Approval)
    * - wvta_code_version
      - string
      - O
      - 
      - WVTA-code version (Whole Vehicle Type Approval)
    * - adr_code
      - string
      - O
      - 
      - ADR code (from French title: Accord européen relatif au transport international des marchandises Dangereuses par Route -> Dangerous Goods Transport)
    * - first_use_date
      - date Y-m-d
      - O
      - 
      - Date of first use
    * - production_year
      - int
      - O
      - 
      - Year of production
    * - registration_date
      - date Y-m-d
      - O
      - 
      - Registration date
    * - plate_number_withdrawal_date
      - date Y-m-d
      - O
      - 
      - License plate withdrawal date
    * - type_div
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5003
      - 5003
    * - vehicle_usage
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5200
      - 5200
    * - property_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5609
      - 5609
    * - fuel_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5015
      - 5015
    * - traction_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=501G
      - 501G
    * - garage
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5420
      - 5420
    * - special_license_plate_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=500J
      - 500J
    * - vehicle_category
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=500G
      - 500G
    * - license_plate_format
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5023
      - 5023
    * - color
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5019
      - 5019
    * - anti_theft_system1
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5634
      - 5634
    * - anti_theft_system2
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5334
      - 5634
    * - anti_theft_system3
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5634
      - 5634
    * - pedaling_assistance
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5026
      - 5026
    * - financing
      - bool
      - O
      - 
      - Financing?
    * - coverage_tax
      - bool
      - O
      - 
      - Tax for putting into circulation covered?
    * - tax
      - float
      - O
      - 
      - Tax on putting into circulation
    * - list_value
      - float
      - O
      - 
      - List value
    * - list_value_vat_included
      - float
      - O
      - 
      - Listed value (VAT incl.)
    * - power
      - int
      - O
      - 
      - Power
    * - engine_capacity
      - float
      - O
      - 
      - Engine capacity
    * - number_of_seats
      - int
      - O
      - 
      - Number of seats
    * - number_of_doors
      - int
      - O
      - 
      - Number of doors
    * - maximum_authorised_weight
      - float
      - O
      - 
      - Maximum authorised weight
    * - tare
      - float
      - O
      - 
      - Tare (weight of an empty vehicle)
    * - weight_in_working_order
      - O
      - 
      - float
      - Operating weight
    * - maximum_weight_of_the_running_gear
      - float
      - O
      - 
      - Maximum weight of the running gear
    * - places
      - int
      - O
      - 
      - Places
    * - percentage_of_vat_exemption      
      - float
      - O
      - 
      - Percentage of VAT exemption
    * - co2
      - float
      - O
      - 
      - CO2
    * - km
      - int
      - O
      - 
      - Mileage
    * - km_date
      - date Y-m-d
      - O
      - 
      - Date of recording mileage
    * - max_km_yearly
      - int
      - O
      - 
      - Maximum number of kilometers / year
    * - div_conform
      - bool
      - O
      - 
      - VRO identifiers unchanged
    * - convertible
      - bool
      - O
      - 
      - Is convertible?
    * - coupe    
      - bool
      - O
      - 
      - Is coupé
    * - fleet
      - bool
      - O
      - 
      - Vehicle belongs to fleet
    * - sportscar
      - bool
      - O
      - 
      - Is sports car
    * - suv
      - bool
      - O
      - 
      - Is SUV
    * - anti_theft_system
      - bool
      - O
      - 
      - Has anti theft system
    * - international_transports
      - bool
      - O
      - 
      - International transport?
    * - speed_pedelec
      - bool
      - O
      - 
      - Vehicle is a Speed pedelec?

Risk Person Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '030'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '030')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, else O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - party_id
      - int
      - O
      - 
      - id of the party insured in the risk_person.

Risk Trailer Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '002'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '002')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - make
      - string
      - O
      - 
      - Make of the trailer
    * - model
      - string
      - O
      - 
      - Model of the trailer
    * - type
      - string
      - O
      - 
      - Type of the trailer
    * - plate_number
      - string
      - O
      - 
      - License plate
    * - chassis_number
      - string
      - O
      - 
      - Chassis number
    * - check_digit
      - string
      - O
      - 
      - Check digit
    * - vin_unifier
      - string
      - O
      - 
      - VIN unifier
    * - pvg_number
      - string
      - O
      - 
      - PVA number
    * - european_pvg_number
      - string
      - O
      - 
      - European PVA number
    * - european_name
      - string
      - O
      - 
      - European name trailer
    * - div_code
      - string
      - O
      - 
      - VRO code (make - model)
    * - wvta_code_number
      - string
      - O
      - 
      - WVTA-code Number (Whole Vehicle Type Approval)
    * - wvta_code_variant
      - string
      - O
      - 
      - WVTA-code variant (Whole Vehicle Type Approval)
    * - wvta_code_version
      - string
      - O
      - 
      - WVTA-code version (Whole Vehicle Type Approval)
    * - first_use_date
      - date Y-m-d
      - O
      - 
      - Date of first use
    * - production_year
      - int
      - O
      - 
      - Year of production
    * - registration_date
      - date Y-m-d
      - O
      - 
      - Registration date
    * - plate_number_withdrawal_date
      - date Y-m-d
      - O
      - 
      - License plate withdrawal date
    * - type_div
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5003
      - 5003
    * - special_license_plate_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=500J
      - 500J
    * - vehicle_category
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=500G
      - 500G
    * - license_plate_format
      - telebib
      - O
      - 
      - 5023
    * - coverage_tax
      - bool
      - O
      - 
      - Tax for putting into circulation covered?
    * - tax
      - float
      - O
      - 
      - Tax on putting into circulation
    * - list_value
      - float
      - O
      - 
      - List value
    * - list_value_vat_included
      - float
      - O
      - 
      - Listed value (VAT incl.)
    * - maximum_authorised_weight
      - float
      - O
      - 
      - Maximum authorised weight
    * - tare
      - float
      - O
      - 
      - Tare (weight of an empty vehicle)
    * - weight_in_working_order
      - float
      - O
      - 
      - Operating weight
    * - maximum_weight_of_the_running_gear
      - float
      - O
      - 
      - Maximum weight of the running gear
    * - percentage_of_vat_exemption
      - float
      - O
      - 
      - Percentage of VAT exemption
    * - div_conform
      - bool
      - O
      - 
      - VRO identifiers unchanged
    * - international_transports
      - bool
      - O
      - 
      - International transport?

Risk Vessel Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type = '003'
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = '003')
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - M in case of update, else O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - O
      - 
      - Name of the vessel
    * - make
      - string
      - O
      - 
      - Make of the vessel
    * - model
      - string
      - O
      - 
      - Model of the vessel
    * - type
      - string
      - O
      - 
      - Type of the vessel
    * - production_year
      - int
      - O
      - 
      - Year of production
    * - address
      - array
      - O
      - 
      - Address where the vessel was constructed. Address Fields
    * - hullidentification
      - string
      - O
      - 
      - Hull identification number
    * - certificate_colours_code
      - string
      - O
      - 
      - Certificate of colours - Code
    * - certificate_of_colours_number
      - string
      - O
      - 
      - Certificate of colours - Number
    * - plate_number
      - string
      - O
      - 
      - License plate
    * - vessel_usage
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5800
      - 5800
    * - vessel_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5801
      - 5801
    * - rigging
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5802
      - 5802
    * - constructionmaterials
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5803
      - 5803
    * - total_length
      - float
      - O
      - 
      - Total length
    * - length
      - float
      - O
      - 
      - Length
    * - width
      - float
      - O
      - 
      - Width
    * - draught
      - float
      - O
      - 
      - Draught
    * - port_of_register
      - string
      - O
      - 
      - Home port
    * - shipyard
      - string
      - O
      - 
      - Shipyard

Risk Other Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - risk_type
      - telebib
      - M
      - https://www.telebib2.org/popup3posQLists.asp?XList=X052
      - X052 (value = all telebib codes not used in the above risk types)
    * - id
      - int
      - N/A, BC will assign
      - 
      - Unique id of the risk endorsement
    * - risk_id
      - int
      - O
      - 
      - unique id of the risk.    
    * - description
      - string
      - O
      - 
      - Description of the risk
    * - current_endorsement
      - bool
      - N/A, every update will result in the current endorsement.
      - 
      - This endorsement is the last and current endorsement. (highest endorsement number)
    * - last_activity
      - date Y-m-d H:i:s
      - O
      - 
      - The last time the risk was opened or edited
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the risk
    * - added_time
      - date Y-m-d H:i:s
      - O
      - 
      - The time the risk was created
    * - parties
      - array
      - O
      - 
      - Risk parties. Parties linked to the risk
    * - name
      - string
      - O
      - 
      - Name
    * - other_description
      - string
      - O
      - 
      - Description

Address Fields
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - Unique id of the address
    * - street 
      - string
      - M
      - 
      - Street
    * - number 
      - string
      - M
      - 
      - Number
    * - bus 
      - string
      - O
      - 
      - Bus
    * - extra
      - string
      - O
      - 
      - Extra
    * - city 
      - string
      - M
      - 
      - city
    * - postal_code 
      - string
      - M
      - 
      - Postal_code
    * - country 
      - string
      - M
      - 
      - Country    
    * - description
      - string
      - M 
      - 
      - Description of the address

Risk parties
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - unique id of the risk_party
    * - risk_version_id
      - int
      - N/A, BC will assign
      - 
      - id of the risk endorsement
    * - party_id
      - int
      - M in case of update, elso O
      - 
      - Unique id of the party    
    * - type
      - string
      - M
      - 
      - Type (Owner, Driver, Insured, ...)
    * - driver_type
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5125
      - 5125
   
        

Guarantees
============

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /guarantees/{id}


Fields general
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the guarantee
    * - policy_version_id
      - int
      - id of the policy endorsement
    * - policy_risk_id
      - int
      - id of the policy_risk (Risk policies)
    * - type
      - telebib
      - A574
    * - formula
      - string
      - Formula
    * - inception_date
      - date Y-m-d
      - Inception date
    * - expiry_date
      - date Y-m-d
      - Expiry date
    * - first_commission
      - date Y-m-d
      - Date first commission
    * - last_commission
      - date Y-m-d
      - Date last commission
    * - response_commercial_action
      - bool
      - Commercial promotion?
    * - exemption
      - bool
      - Is there an exemption
    * - exemption_surrender
      - bool
      - Is the exemption surrendered
    * - capital
      - float
      - Amount capital
    * - change_of_capital
      - float
      - Change of capital
    * - capital_per_object
      - float
      - Capital per object
    * - base_premium
      - float
      - Base premium
    * - technical_expenses
      - float
      - Technical expenses
    * - commercialization_expenses
      - float
      - Commercialization costs
    * - net_premium
      - float
      - Net premium
    * - commission
      - float
      - Commission
    * - costs
      - float
      - Costs
    * - charges
      - float
      - Charges
    * - costs_and_charges
      - float
      - Costs and charges
    * - other_costs_added_to_net_premium
      - float
      - Other costs to be added to the net premium
    * - splitting_costs
      - float
      - Split costs
    * - estimated_acquisition_costs
      - float
      - Estimated acquisition costs
    * - estimated_administration_costs
      - float
      - Estimated administration costs
    * - premium_rate
      - float
      - Premium rate
    * - commission_percentage
      - float
      - Commission percentage
    * - charges_percentage
      - float
      - Charges percentage
    * - costs_percentage
      - float
      - Costs percentage
    * - charges_and_costs_percentage
      - float
      - Charges and costs percentage
    * - commercial_discount_percentage
      - float
      - Commercial discout percentage
    * - net_investment_percentage
      - float
      - Net investment percentage
    * - subscription_index
      - float
      - Subscription index
    * - instance_name
      - string
      - Name of the instance
    * - url
      - string
      - URL
    * - exemptions
      - array
      - Guarantee exemptions

Additional Fields motorvehicle
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - actual_no_claims_bonus_level
      - telebib
      - 5300
    * - reference_level_for_no_claims_bonus
      - telebib
      - 5340
    * - previous_no_claims_bonus_level
      - telebib
      - 5310
    * - actual_no_claims_bonus_class
      - telebib
      - 5301
    * - previous_no_claims_bonus_class
      - telebib
      - 5302
    * - referenced_no_claims_bonus_class
      - telebib
      - 5303
    * - type_of_value_covered
      - telebib
      - 5400
    * - reduced_percentage_for_other_insurance
      - float
      - Percentage reduction for plurality

Additional Fields fire
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - compensation_days
      - int
      - Days compensated
    * - compensation_weeks
      - int
      - Weeks compensated
    * - compensation_months
      - int
      - Months compensated
    * - value_type_covered_theft
      - telebib
      - 3464
    * - payment_type_economic_damage
      - telebib
      - 34B6
    * - consequence_cession_of_recourse
      - telebib
      - M03K
    * - legal_deductable_applied
      - bool
      - Application of general contractual exemption?
    * - appeal_dropped
      - bool
      - Appeal dropped?
    * - amount_first_risk
      - float
      - Amount fist risk
    * - capital_daily_allowance
      - float
      - Capital - Daily allowance
    * - number_deductible_applied
      - int
      - Number of times legal exemption is applied
    * - percentage_insured_value
      - float
      - Percentage of insured value
    * - percentage_intervention
      - float
      - Percentage intervention

Additional Fields Personal
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - compensation_length_days
      - int
      - duration of compensation in days
    * - compensation_length_weeks
      - int
      - duration of compensation in weeks
    * - compensation_length_months
      - int
      - duration of compensation in months
    * - type_payment_long_term_disability
      - telebib
      - 2200
    * - agreed_subrogation
      - bool
      - Agreed subrogation?
    * - death_benefit
      - float
      - Capital upon death
    * - capital_daily_allowance
      - float
      - Capital - Daily allowance
    * - capital_maximum_benefit
      - float
      - Capital - Maximum payment
    * - capital_annual_compensation
      - float
      - Capital - Annual payment

Additional Fields AO (W.Comp. - Workers Compensation)
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - premium_adaptable_to_risk_evolution 
      - int
      - Is the premium adjustable to the evolution of the risk?
    * - premium_per_unit
      - float
      - Premium per unit
    * - premium_rate_ao
      - float
      - Premium rate
       
Guarantee exemptions
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the account number
    * - exemption_type
      - telebib
      - X093
    * - expressed_as
      - telebib
      - P11T
    * - calculation_base
      - telebib
      - P11U
    * - type_excess_informex
      - telebib
      - CP01
    * - surrender_unrefunded_sick_leave_period
      - bool
      - Surrender unrefunded sick leave period?
    * - unrefunded_period_maintained_above_60_years
      - bool
      - Retention of own risk period after 60 years?
    * - exemption_surrender
      - bool
      - Surrender exemption?
    * - contractual_exemption
      - bool
      - Contractual exemption?
    * - threshold_exemption
      - bool
      - Threshold exemption?
    * - young_drivers_exemption
      - bool
      - Exemption young driver?
    * - territorial_exemption
      - bool
      - Territorial exemption?
    * - deposited_exemption
      - bool
      - Deposited exemption?
    * - corporal_damages
      - bool
      - Exemption in physical damage?
    * - material_damages
      - bool
      - Exemption in material damage?
    * - mixed_damages
      - bool
      - Exemption in mixed damage?
    * - immaterial_damages
      - bool
      - Exemption for non-material damage?
    * - entrusted_objects
      - bool
      - Exemption in entrusted objects
    * - exemption
      - float
      - Amount exemption
    * - minimal_value
      - float
      - Minimum value
    * - maximal_value
      - float
      - Maximum value
    * - percentage_deductable_insured_value
      - float
      - Percentage exemption on insured value
    * - percentage_deductable_damage_amount
      - float
      - Percentage exemption on damage amount
    * - waiting_delay_days
      - int
      - Waiting delay in days
    * - waiting_delay_weeks
      - int
      - Waiting delay in weeks
    * - waiting_delay_months
      - int
      - Waiting delay in months

GET
------

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - policy_version_id
      - int
      - id of the policy
    * - risk_version_id
      - int
      - id of the risk
    * - changed_since
      - unix timestamp
      - the guarantee changed since this date

**Headers**

The HTTP header X-OFFICE-ID is required for this endpoint.

**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json

  {
    "id":1,
    "policy_version_id":1,
    "policy_risk_id":1,
    "type": {"code":"520", "description":"Rechtsbijstand (Voertuig)"},
    "formula":"R002",
    "inception_date":"2019-04-10",
    "expiry_date":null,
    "first_commission":null,
    "last_commission":null,
    "response_commercial_action":null,
    "exemption":null,
    "exemption_surrender":null,
    "capital":null,
    "change_of_capital":null,
    "capital_per_object":null,
    "base_premium":45.10,
    "technical_expenses":1.92,
    "commercialization_expenses":19.99,
    "net_premium":62.69,
    "commission":15.67,
    "costs":0.00,
    "charges":10.50,
    "costs_and_charges":10.50,
    "other_costs_added_to_net_premium":null,
    "splitting_costs":null,
    "estimated_acquisition_costs":18.07,
    "estimated_administration_costs":1.92,
    "premium_rate":null,
    "commission_percentage":25.00,
    "charges_percentage":16.75,
    "costs_percentage":null,
    "charges_and_costs_percentage":16.75,
    "commercial_discount_percentage":null,
    "net_investment_percentage":null,
    "subscription_index":null,
    "instance_name":null,
    "url":null,
    "exemption":[{"id":1,"type":{"code":"003","description":"Overeengekomen bedrag per schadegeval"},"type":{"code":"2","description":"Bedrag"},"calculation_base":null,"type_excess_informex":null,"surrender_unrefunded_sick_leave_period":0,"unrefunded_period_maintained_above_60_years":0,"exemption_surrender":0,"contractual_exemption":1,"threshold_exemption":0,"young_drivers_exemption":0,"territorial_exemption":0,"deposited_exemption":0,"corporal_damages":0,"material_damages":0,"mixed_damages":0,"immaterial_damages":0,"entrusted_objects":0,"exemption":0.00,"minimal_value":0.00,"maximal_value":0.00,"percentage_deductable_insured_value":0.00,"percentage_deductable_damage_amount":0.00,"waiting_delay_days":0,"waiting_delay_weeks":0,"waiting_delay_months":0}],
    "actual_no_claims_bonus_level":null,
    "reference_level_for_no_claims_bonus":null,
    "previous_no_claims_bonus_level":null,
    "actual_no_claims_bonus_class":null,
    "previous_no_claims_bonus_class":null,
    "referenced_no_claims_bonus_class":null,
    "type_of_value_covered":null,
    "reduced_percentage_for_other_insurance":null
  }


POST (in progress)
------

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /guarantees


Fields general
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - Not allowed, Brokercloud will assign.
      - 
      - Unique id of the guarantee
    * - policy_version_id
      - int
      - M
      - 
      - id of the policy endorsement
    * - policy_risk_id
      - int
      - M
      - 
      - id of the policy_risk (Risk policies)
    * - type
      - telebib
      - M
      - 
      - A574
    * - formula
      - string
      - O
      - 
      - Formula
    * - inception_date
      - date Y-m-d
      - M
      - 
      - Inception date
    * - expiry_date
      - date Y-m-d
      - O
      - 
      - Expiry date
    * - first_commission
      - date Y-m-d
      - O
      - 
      - Date first commission
    * - last_commission
      - date Y-m-d
      - O
      - 
      - Date last commission
    * - response_commercial_action
      - bool
      - O
      - 
      - Commercial promotion?
    * - exemption
      - bool
      - O
      - 
      - Is there an exemption
    * - exemption_surrender
      - bool
      - O
      - 
      - Is the exemption surrendered
    * - capital
      - float
      - O
      - 
      - Amount capital
    * - change_of_capital
      - float
      - O
      - 
      - Change of capital
    * - capital_per_object
      - float
      - O
      - 
      - Capital per object
    * - base_premium
      - float
      - O
      - 
      - Base premium
    * - technical_expenses
      - float
      - O
      - 
      - Technical expenses
    * - commercialization_expenses
      - float
      - O
      - 
      - Commercialization costs
    * - net_premium
      - float
      - 
      - 
      - Net premium
    * - commission
      - float
      - O
      - 
      - Commission
    * - costs
      - float
      - O
      - 
      - Costs
    * - charges
      - float
      - O
      - 
      - Charges
    * - costs_and_charges
      - float
      - O
      - 
      - Costs and charges
    * - other_costs_added_to_net_premium
      - float
      - O
      - 
      - Other costs to be added to the net premium
    * - splitting_costs
      - float
      - O
      - 
      - Split costs
    * - estimated_acquisition_costs
      - float
      - O
      - 
      - Estimated acquisition costs
    * - estimated_administration_costs
      - float
      - O
      - 
      - Estimated administration costs
    * - premium_rate
      - float
      - O
      - 
      - Premium rate
    * - commission_percentage
      - float
      - O
      - 
      - Commission percentage
    * - charges_percentage
      - float
      - O
      - 
      - Charges percentage
    * - costs_percentage
      - float
      - O
      - 
      - Costs percentage
    * - charges_and_costs_percentage
      - float
      - O
      - 
      - Charges and costs percentage
    * - commercial_discount_percentage
      - float
      - O
      - 
      - Commercial discout percentage
    * - net_investment_percentage
      - float
      - O
      - 
      - Net investment percentage
    * - subscription_index
      - float
      - O
      - 
      - Subscription index
    * - instance_name
      - string
      - O
      - 
      - Name of the instance
    * - url
      - string
      - O
      - 
      - URL
    * - exemptions
      - array
      - O
      - 
      - Guarantee exemptions

Additional Fields motorvehicle
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - actual_no_claims_bonus_level
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5300
      - 5300
    * - reference_level_for_no_claims_bonus
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5340
      - 5340
    * - previous_no_claims_bonus_level
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5310
      - 5310
    * - actual_no_claims_bonus_class
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5310
      - 5301
    * - previous_no_claims_bonus_class
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5302
      - 5302
    * - referenced_no_claims_bonus_class
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5303
      - 5303
    * - type_of_value_covered
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=5400
      - 5400
    * - reduced_percentage_for_other_insurance
      - float
      - O
      - 
      - Percentage reduction for plurality

Additional Fields fire
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - compensation_days
      - int
      - O
      - 
      - Days compensated
    * - compensation_weeks
      - int
      - O
      - 
      - Weeks compensated
    * - compensation_months
      - int
      - O
      - 
      - Months compensated
    * - value_type_covered_theft
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=3464
      - 3464
    * - payment_type_economic_damage
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=34B6
      - 34B6
    * - consequence_cession_of_recourse
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=M03K
      - M03K
    * - legal_deductable_applied
      - bool
      - O
      - 
      - Application of general contractual exemption?
    * - appeal_dropped
      - bool
      - O
      - 
      - Appeal dropped?
    * - amount_first_risk
      - float
      - O
      - 
      - Amount fist risk
    * - capital_daily_allowance
      - float
      - O
      - 
      - Capital - Daily allowance
    * - number_deductible_applied
      - int
      - O
      - 
      - Number of times legal exemption is applied
    * - percentage_insured_value
      - float
      - O
      - 
      - Percentage of insured value
    * - percentage_intervention
      - float
      - O
      - 
      - Percentage intervention

Additional Fields Personal
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - compensation_length_days
      - int
      - O
      - 
      - duration of compensation in days
    * - compensation_length_weeks
      - int
      - O
      - 
      - duration of compensation in weeks
    * - compensation_length_months
      - int
      - O
      - 
      - duration of compensation in months
    * - type_payment_long_term_disability
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=2200
      - 2200
    * - agreed_subrogation
      - bool
      - O
      - 
      - Agreed subrogation?
    * - death_benefit
      - float
      - O
      - 
      - Capital upon death
    * - capital_daily_allowance
      - float
      - O
      - 
      - Capital - Daily allowance
    * - capital_maximum_benefit
      - float
      - O
      - 
      - Capital - Maximum payment
    * - capital_annual_compensation
      - float
      - O
      - 
      - Capital - Annual payment

Additional Fields AO (W.Comp. - Workers Compensation)
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - premium_adaptable_to_risk_evolution 
      - int
      - O
      - 
      - Is the premium adjustable to the evolution of the risk?
    * - premium_per_unit
      - float
      - O
      - 
      - Premium per unit
    * - premium_rate_ao
      - float
      - O
      - 
      - Premium rate
       
Guarantee exemptions
^^^^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - Not allowed, BC will assign.
      - 
      - unique id of the account number
    * - exemption_type
      - telebib
      - M
      - 
      - X093
    * - expressed_as
      - telebib
      - M
      - https://www.telebib2.org/CodeListsValues.asp?waarde=P11T
      - P11T
    * - calculation_base
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=P11U
      - P11U
    * - type_excess_informex
      - telebib
      - O
      - https://www.telebib2.org/CodeListsValues.asp?waarde=CP01
      - CP01
    * - surrender_unrefunded_sick_leave_period
      - bool
      - O
      - 
      - Surrender unrefunded sick leave period?
    * - unrefunded_period_maintained_above_60_years
      - bool
      - O
      - 
      - Retention of own risk period after 60 years?
    * - exemption_surrender
      - bool
      - O
      - 
      - Surrender exemption?
    * - contractual_exemption
      - bool
      - O
      - 
      - Contractual exemption?
    * - threshold_exemption
      - bool
      - O
      - 
      - Threshold exemption?
    * - young_drivers_exemption
      - bool
      - O
      - 
      - Exemption young driver?
    * - territorial_exemption
      - bool
      - O
      - 
      - Territorial exemption?
    * - deposited_exemption
      - bool
      - O
      - 
      - Deposited exemption?
    * - corporal_damages
      - bool
      - O
      - 
      - Exemption in physical damage?
    * - material_damages
      - bool
      - O
      - 
      - Exemption in material damage?
    * - mixed_damages
      - bool
      - O
      - 
      - Exemption in mixed damage?
    * - immaterial_damages
      - bool
      - O
      - 
      - Exemption for non-material damage?
    * - entrusted_objects
      - bool
      - O
      - 
      - Exemption in entrusted objects
    * - exemption
      - float
      - O
      - 
      - Amount exemption
    * - minimal_value
      - float
      - O
      - 
      - Minimum value
    * - maximal_value
      - float
      - O
      - 
      - Maximum value
    * - percentage_deductable_insured_value
      - float
      - O
      - 
      - Percentage exemption on insured value
    * - percentage_deductable_damage_amount
      - float
      - O
      - 
      - Percentage exemption on damage amount
    * - waiting_delay_days
      - int
      - O
      - 
      - Waiting delay in days
    * - waiting_delay_weeks
      - int
      - O
      - 
      - Waiting delay in weeks
    * - waiting_delay_months
      - int
      - O
      - 
      - Waiting delay in months


Folders
============

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /folders/{id}


Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - unique id of the folder
    * - office_id
      - int
      - Unique id of the brokers office the folder belongs to
    * - name
      - string
      - Name of the folder
    * - party_id
      - int
      - id of the party
    * - policy_id
      - int
      - id of the policy
    * - claim_id
      - int
      - id of the claim
    * - risk_id
      - int
      - id of the risk
    * - in_folder_id
      - int
      - id of the parent folder
    * - webdav_user_id
      - int
      - id of the webdav_user
    * - added_by
      - Brokercloud user
      - The user that created the folder
    * - added_date
      - date Y-m-d H:i:s
      - The time the folder was created
    * - updated_by
      - Brokercloud user
      - The user that last updated the folder
    * - updated_date
      - date Y-m-d H:i:s
      - The time the folder was last updated

Files
============

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /files/{id}


Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the file
    * - office_id
      - int
      - Unique id of the brokers office the file belongs to
    * - party_id
      - int
      - id of the party
    * - policy_id
      - int
      - id of the policy
    * - policy_version_id
      - int
      - id of the policy endorsement (if the file is linked to the endorsement only)
    * - claim_id
      - int
      - id of the claim
    * - risk_id
      - int
      - id of the risk
    * - risk_version_id
      - int
      - id of the risk endorsement (if the file is linked to the endorsement only)
    * - folder_id
      - int
      - id of the folder
    * - webdav_user_id
      - int
      - id of the webdav user
    * - name
      - string
      - Name of the file
    * - description
      - string
      - Description of the file
    * - type
      - string
      - mime type of the file
    * - content
      - string
      - base64 encoded content of the file. This field is only available when one file is requested.
    * - size
      - int
      - Size of the file in bytes
    * - document_date
      - date Y-m-d
      - Document date
    * - added_by
      - Brokercloud user
      - The user that created the file
    * - added_date
      - date Y-m-d H:i:s
      - The time the file was created
    * - updated_by
      - Brokercloud user
      - The user that last updated the file
    * - updated_date
      - date Y-m-d H:i:s
      - The time the file was last updated

GET
------

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - party_id
      - int
      - id of the party
    * - policy_id
      - int
      - id of the policy
    * - claim_id
      - int
      - id of the claim
    * - risk_id
      - int
      - id of the risk
    * - folder_id
      - int
      - id of the folder
    * - changed_since
      - unix timestamp
      - the file changed since this date

**Headers**

The HTTP header X-OFFICE-ID is required for this endpoint.

**Example request**:

.. sourcecode:: http

  GET /files/1 HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg
  
  
**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json
  
  {
    "id":1,
    "name":"file.pdf",
    "description":"A file",
    "type":"application/pdf",
    "content":"...",
    "size":50000,
    "document_date":"2019-01-01",
    "added_time":"2019-01-01",
    "added_by":{"id":1, "fistname":"Lothar", "lastname":"Hinckxt"},
    "edited_time":null,
    "edited_by":null,
  }


POST (in progress)
------

Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - M in case of update, else O
      - 
      - Unique id of the file
    * - party_id
      - int
      - O (but at least one link should be present)
      - 
      - id of the party
    * - policy_id
      - int
      - O (but at least one link should be present)
      - 
      - id of the policy
    * - policy_version_id
      - int
      - O (but at least one link should be present)
      - 
      - id of the policy endorsement (if the file is linked to the endorsement only)
    * - claim_id
      - int
      - O (but at least one link should be present)
      - 
      - id of the claim
    * - risk_id
      - int
      - O (but at least one link should be present)
      - 
      - id of the risk
    * - risk_version_id
      - int
      - O (but at least one link should be present)
      - 
      - id of the risk endorsement (if the file is linked to the endorsement only)
    * - folder_id
      - int
      - O (but at least one link should be present)
      - 
      - id of the folder
    * - webdav_user_id
      - int
      - O
      - 
      - id of the webdav user
    * - name
      - string
      - M
      - 
      - Name of the file
    * - description
      - string
      - O
      - 
      - Description of the file
    * - type
      - string
      - M
      - 
      - mime type of the file
    * - content
      - string
      - M
      - 
      - base64 encoded content of the file.
    * - size
      - int
      - M
      - 
      - Size of the file in bytes
    * - document_date
      - date Y-m-d
      - O
      - 
      - Document date
    * - added_by
      - Brokercloud user
      - O
      - 
      - The user that created the file



Offices
============

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /offices/{id}


Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the office
    * - name
      - string
      - name of the office
    * - vat_number
      - string
      - vat number of the office
    * - fsma
      - string
      - fsma of the office
    * - phone
      - string
      - phone number of the office
    * - email
      - string
      - email number of the office
    * - website
      - string
      - website number of the office
    * - active_from
      - date Y-m-d
      - date the office became active
    * - active_until
      - date Y-m-d
      - date the office became inactive
    * - logo_id
      - int
      - file_id of the logo
GET
------

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - active
      - bool
      - active offices or all offices

**Example request**:

.. sourcecode:: http

  GET /offices/1 HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg
  
  
**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json
  
  {
    "id":1,
    "name":"Kantoor X",
    "vat_number":"BE0680574071",
    "fsma":"0680574071",
    "phone":"035500000",
    "email":"hello@brokercloud.eu",
    "website":"brokercloud.app",
    "active_from":"2019-01-01",
    "active_until":null,
    "logo_id":1,
  }

Logs
============

.. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /logs/{id}


Fields
--------------

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Description
    * - id
      - int
      - Unique id of the log
    * - user_id
      - int
      - user_id of the user who performed the action
    * - office_id
      - int
      - office_id of the affected resource
    * - time
      - date Y-m-d H:i:s
      - time the action occured
    * - type
      - enum c,r,u,d (create, read, update, delete)
      - the action that was performed
    * - description 
      - string
      - Description of the resource
    * - party_id
      - int
      - party_id of the affected resource
    * - policy_id
      - int
      - policy_id of the affected resource
    * - policy_version_id
      - int
      - policy_version_id of the affected resource
    * - risk_id
      - int
      - risk_id of the  affected resource
    * - risk_version_id
      - int
      - risk_version_id of the affected resource
    * - claim_id
      - int
      - claim_id of the affected resource
    * - guarantee_id
      - int
      - guarantee_id of the affected resource
GET
------

**Parameters**

.. list-table::
    :header-rows: 1
    :align: left

    * - Parameter
      - Type
      - Description
    * - user_id
      - int
      - the user_id of the user who performed the action
    * - type
      - enum c,r,u,d (create, read, update, delete)
      - the action that was performed
    * - changed_since
      - unix timestamp
      - the action was performed after this time

**Example request**:

.. sourcecode:: http

  GET /logs/1 HTTP/1.1
  Host: secure.brokercloud.app
  Content-Type: application/json
  Authorization: Bearer NTYyNzMzNmI2OTZjMmM1MzgwZTc3MjBhZDdlZTNmMThjOTdmOWU0NmM0NWM0YzkxNTc2ZDQzMjM4NDFlMjRiMg
  
  
**Example response**:

.. sourcecode:: http
  
  HTTP/1.1 200 OK
  Vary: Accept
  Content-Type: application/json
  
  {
        "id":30,
        "user_id":1,
        "office_id":1,
        "type":"d",
        "time":"2019-07-25 12:57:43",
        "description":"Nikki VERSCHUEREN",
        "party_id":1354011,
        "policy_id":null,
        "policy_version_id":null,
        "risk_id":null,
        "risk_version_id":null,
        "claim_id":null,
        "guarantee_id":null
  } 





POST (in progress)
--------------

  .. list-table::
    :header-rows: 1

    * - Description
      - URL
    * - Endpoint
      - /logs

.. list-table::
    :header-rows: 1
    :align: left

    * - Field
      - Type
      - Mandatory / Optional
      - Possible values
      - Description
    * - id
      - int
      - Not allowed, BC will assign.
      - 
      - Unique id of the log
    * - user_id
      - int
      - O (maybe best to create technical users for applicatication if we allow logging)
      - 
      - user_id of the user who performed the action
    * - office_id
      - int
      - M
      - 
      - office_id of the affected resource
    * - time
      - date Y-m-d H:i:s
      - M
      - 
      - time the action occured
    * - type
      - enum c,r,u,d (create, read, update, delete)
      - M
      - 
      - the action that was performed
    * - description 
      - string
      - M
      - 
      - Description of the resource
    * - party_id
      - int
      - O (but at least one link should be present)
      - 
      - party_id of the affected resource
    * - policy_id
      - int
      - O (but at least one link should be present)
      - 
      - policy_id of the affected resource
    * - policy_version_id
      - int
      - O (but at least one link should be present)
      - 
      - policy_version_id of the affected resource
    * - risk_id
      - int
      - O (but at least one link should be present)
      - 
      - risk_id of the  affected resource
    * - risk_version_id
      - int
      - O (but at least one link should be present)
      - 
      - risk_version_id of the affected resource
    * - claim_id
      - int
      - O (but at least one link should be present)
      - 
      - claim_id of the affected resource
    * - guarantee_id
      - int
      - O (but at least one link should be present)
      - 
      - guarantee_id of the affected resource
